<b><u>VISUALIZZAZIONE DELLE INFORMAZIONI 2021-2022: secondo progetto Hanout-De Rozeris</u></b>

<b>DATASET:</b><br>
Per il progetto è stato utilizzato un dataset messo a disposizione da Grouplens che contiene informazioni su valutazioni di film da parte degli utenti. 
Il dataset è stato già utilizzato per un altro progetto (sistema di raccomandazione) di un altro corso, da cui si è partiti per l'implementazione di questo nuovo sistema.

In particolare il dataset contiene diverse entità:<br>
1.	Movie (movieId,title,genres)<br>
2.	Rating (userId,movieId,rating,timestamp)<br>
3.	Tag (userId,movieId,tag,timestamp)<br>
4.	Link (movieId,imdbId,tmdbId): identificatori di altre fonti esterne<br>

Per i nostri scopi, sono stati utilizzati i dati inerenti i Movie e i Rating.

<b>OBIETTIVO:</b><br>
Lo scopo del progetto è stato quello di realizzare un sistema in grado di fornire delle informazioni rilevanti all'utente target. 
L'utente target è una tipologia di utente che collabora con delle piattaforme streaming (Prime Video, Netflix, …) e che deve individuare i generi e/o i film più apprezzati, 
in generale o in riferimento ad un determinato intervallo temporale (un anno, un mese). 
Con le informazioni estrapolate dal sistema l'utente potrà, ad esempio, creare un catalogo con i film più apprezzati di sempre e aggiornarlo periodicamente secondo i trend del momento.

<b>TRASFORMAZIONE DATASET:</b><br>
Inizialmente, si sono rese necessarie delle trasformazioni del dataset iniziale: 
in particolare, è stata creata una tabella di relazione (idMovie, idGenre) utile a semplificare le query necessarie ai nostri scopi.

<b>DESCRIZIONE SISTEMA E FUNZIONALITA' IMPLEMENTATE:</b><br>
•	Il sistema realizzato è un sistema dove gli utenti registrati hanno effettuato una serie di valutazioni di film, ognuno dei quali ha uno o più generi.
 
•	L’utente target, nel sistema, può visualizzare l’apprezzamento dei diversi generi, 
prima nel complesso (ovvero tenendo conto di tutte le valutazioni nel tempo) oppure, 
mediante un’interazione, visualizzare le informazioni relative ad un determinato anno o mese. 
In particolare, è stato realizzato un grafico a barre che permette di filtrare per anno e successivamente per mese, 
consentendo quindi di visualizzare le informazioni a diversi livelli di dettaglio.
Ciascuna barra identifica un genere e il valore sull'asse delle Y ne rappresenta l'apprezzamento medio. 
E' inoltre presente un tooltip che, al passaggio del mouse su una determinata barra, mostra la valutazione media e il numero di rating che hanno determinato quel valore.
 
•	Per un determinato livello di dettaglio, l’utente target può selezionare (e deselezionare) una o più barre, ognuna relativa ad un genere: 
il sistema, in base alle selezioni degli utenti e mediante uno scatterplot mostrerà i film di almeno uno dei generi selezionati.
In particolare, sull'asse delle X viene rappresentato il numero di rating mentre sull'asse delle Y viene rappresentato l'apprezzamento medio.
Per colorare i punti che rappresentano i film, si è deciso di utilizzare una sequential colormap, con il seguente criterio: quanto più il film è associabile a diversi generi, tanto più il punto sarà scuro. 
L'obiettivo di questa scelta è quello di far risaltare i film che potenzialmente possono "coprire" i gusti della maggior parte degli utenti finali piuttosto che quelli appartenenti ad un solo genere e quindi apprezzati da una minoranza di pubblico.
Anche per questa rappresentazione è presente un tooltip che, al passaggio del mouse su ciascun punto, mostrerà il titolo del film, il voto medio, il numero di votazioni e l'elenco dei generi di quel film.
 
•	Con le funzionalità descritte, l’utente target sarà in grado di capire su quali generi conviene “investire” mese per mese e nello specifico su quali titoli gli utenti si sono espressi più favorevolmente ma potrà anche individuare quali sono i film meno apprezzati dei generi meno apprezzati (allo scopo di eliminarli dal catalogo dei film selezionati).

<b>POSSIBILI EVOLUZIONI:</b><br>
Per l'utente target potrebbe essere utile integrare le informazioni sul catalogo attuale della piattaforma con cui collabora:
in questo modo il sistema potrebbe evidenziare i film che fanno o non fanno parte del catalogo e di conseguenza facilitare le scelte dell'utente target.
Inoltre, potrebbe essere gestito la creazione e l'aggiornamento del catalogo stesso.

<b>TECNOLOGIE UTILIZZATE:</b><br>
La web application è stata realizzata interamente in stile Java EE con pattern di progettazione MVC.
La webapp si interfaccia al database MySQL versione 8.0 e utilizza per il frontend delle pagine JSP, scritte in codice HTML, JavaScript, JQuery con l'utilizzo della libreria d3.js per la visualizzazione dei grafici.
Per la parte backend viene utilizzata una Servlet, come ruolo di Controller, per gestire le azioni  e i cambiamenti di stato dell'intera web application.
Infine, è stato utilizzato il framework Hibernate di Spring per gestire un interfacciamento efficente al database.