<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<form id="valuta-form" action="/RecommenderSystem/gestione" method="POST">	
<input id="method" name="method" value="valuta" data-required='false' type="hidden">			
<input id="moviesId" name="moviesId" value="" data-required='false' type="hidden">
<input id="userId" name="userId" value="${userId}" data-required='false' type="hidden">			
<div style="padding: 0px" class="sottoDivNews container panel panel-default panel-table">
<div style="background-color: #fff176;" class="panel-heading">
	<h3 class="titleTable panel-title"><strong>Devi ancora valutare</strong></h3>	
</div>
<section class="divNews search-sec">
              <div class="sottoDivNews container">	
                   <div class="col-lg-8 col-md-8 col-sm-12 p-0">
                        <input autocomplete="off" id="titolofiltro" name="titolofiltro" value="${valTitoloFiltro}" type="text" class="form-control" placeholder="Titolo">
                   </div> 	
                   <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                      		<button type="button" onclick="clickCerca($('#titolofiltro').val())" class="btn btn-primary wrn-btn">Cerca</button>
                   </div>	
               </div>
</section>
<table id="valutaTable" class="customTable table table-striped table-bordered table-list">
<thead>
	<tr id="0">
		<td align="center"><strong>Titolo</strong></td>
		<td align="center"><strong>Genere</strong></td>
		<td align="center" style="width: 240px"><strong>Valutazione</strong></span></td>										
	</tr>
</thead>
<tbody>									
		<c:forEach var="movie" items="${moviesNotRatedByUser}">
			<tr>
				<td id="titolo${movie.id}" align="center" class="verticalMiddle">${movie.titolo}</td>		
				<td id="genere${movie.id}" align="center" class="verticalMiddle">${movie.genere}</td>	
				<td align="center">
					<div class="rating">
					      <input onclick=clickRating(${movie.id}) type="radio" id="star${movie.id}1" name="rating${movie.id}" value="5" /><label for="star${movie.id}1">5 stars</label>
					      <input onclick=clickRating(${movie.id}) type="radio" id="star${movie.id}2" name="rating${movie.id}" value="4" /><label for="star${movie.id}2">4 stars</label>
					      <input onclick=clickRating(${movie.id}) type="radio" id="star${movie.id}3" name="rating${movie.id}" value="3" /><label for="star${movie.id}3">3 stars</label>
					      <input onclick=clickRating(${movie.id}) type="radio" id="star${movie.id}4" name="rating${movie.id}" value="2" /><label for="star${movie.id}4">2 stars</label>
					      <input onclick=clickRating(${movie.id}) type="radio" id="star${movie.id}5" name="rating${movie.id}" value="1" /><label for="star${movie.id}5">1 star</label>
					</div>
				</td>
			</tr>	
		</c:forEach>	
	</tbody>
</table> 	
<div class="panel-footer" style="display: flow-root;">
		<input class="accedi btn btn-lg btn-primary btn-block" tyle="float: right;" type="submit" value="Applica">
	</div>
</div>
</form>
</body>
</html>