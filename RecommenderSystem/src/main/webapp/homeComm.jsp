<!DOCTYPE html>
<meta charset="utf-8">
<style>
</style>
<jsp:include page="includes/headforD3.jsp" />
<jsp:include page="includes/header.jsp" />

<body class="body" background="images/sfondo.jpg">
	<!-- Initialize a select button -->
	<div class="container" style="display: grid; margin-top: -1%;">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h2 class="panel-title" style="font-size: 24px;">Trend valutazioni</h2>
			</div>
			<div class="panel-body">

				<div class="row">
					<div class="col-sm-2">
						<!--  	<p><h3>Filtra per anno</h3></p> -->
						<select data-style="btn-info" id="selectButton"
							class="form-control"></select>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12">
						<div id="monthSelectionDiv" class="input-group"></div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12">
						<div id="bar-chart" class="chart" style="text-align: center;">
							<!-- 		        <div class="title">Genres chart</div> -->
						</div>
						<br>
						<div id="movie-chart" class="chart">
							<!-- 		        <div class="title">Movies detail</div> -->
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>


</body>