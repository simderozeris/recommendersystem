<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="includes/head.jsp"/>
<%@ page isELIgnored="false" %>
<jsp:include page="includes/header.jsp"/>
<body background="images/sfondo.jpg">
<script type="text/javascript">
$(function(){
	 
	 $('#registra').submit(function(event) {
		 var error = validate($('#registra'));
		 if (error != false) {
		 event.preventDefault();
		 }
	 }); 	 
		var dateCheck1 = /^\d{2}$/;
		var dateCheck2 = /^\d{2}\/\d{2}$/;
		var d = new Date();
		$("#form_datetime").datetimepicker({
			format:'dd/mm/yyyy',
	        startView: 'month',
	        minView: 'month',
			autoclose : true,
			todayBtn : true
		});	
		
		$('#data').off();
		
		$('#data').bind('keypress', function (e) {
	        return !(e.which != 8 && e.which != 0 &&
	                (e.which < 48 || e.which > 57) && e.which != 46);
	    });
		
		
		$("#data").on("change paste keyup", function(e) {
			if (e.which == 0 || (e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105)){
				if(dateCheck1.test($("#data").val()) || dateCheck2.test($("#data").val())){
					$("#data").val($("#data").val() + "/");
				}
			}
		});			 
});

function validate(form) {
	var error = false;
	
	form.find('input').each(function(index, input) {
		var name = $(input).attr('name');	
		$('#' + name + 'DIV').removeClass("has-error");
		$('#wrong' + name).text("");
	});
	
	form.find('select').each(function(index, input) {
		var name = $(input).attr('name');	
		$('#' + name + 'DIV').removeClass("has-error");
		$('#wrong' + name).text("");
	});
	
	form.find('textarea').each(function(index, input) {
		var name = $(input).attr('name');	
		$('#' + name + 'DIV').removeClass("has-error");
		$('#wrong' + name).text("");
	});

	
	form.find('input').each(function(index,input) {
		var inputValue = $(input).val();
		var required = $(input).data('required');
		var format = $(input).data('format');
		var name = $(input).attr('name');
		
		if (required == true && inputValue == "") {
			
			$('#' + name + 'DIV').addClass("form-group has-error");
	
			$('#wrong' + name).text("Il campo " + name + " e' obbligatorio");
			
			$("#" + name).focus(function () {
				$('#' + name + 'DIV').removeClass("form-group has-error");
				$('#wrong' + name).text("");
	        });
			
			error = "error";
					
		}
	});
	
	form.find('select').each(function(index,input) {
		var inputValue = $(input).val();
		var required = $(input).data('required');
		var format = $(input).data('format');
		var name = $(input).attr('name');
		
		if (required == true && inputValue == "seleziona") {
			
			$('#' + name + 'DIV').addClass("form-group has-error");
	
			$('#wrong' + name).text("Il campo " + name + " e' obbligatorio");
			
			$("#" + name).focus(function () {
				$('#' + name + 'DIV').removeClass("form-group has-error");
				$('#wrong' + name).text("");
	        });
			
			error = "error";			
		}
	});
	
	return error;
}


</script>	
	<div class="container">
		<div class="row vertical-offset-50">
			<div class="col-md-6 col-md-offset-3">			
				<div id = "panelRegistra" class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Anagrafica</h3>
					</div>
					<div class="panel-body">
						<form id="registra" action="/RecommenderSystem/gestione" method="POST">						
							<input id="method" name="method" value="anagrafica" data-required='false' type="hidden">	
							<input id="userId" name="userId" value="${userId}" data-required='false' type="hidden">									
							<fieldset>
								
								<div id="nomeDIV"  class="input-group col-md-12">
									<input type="text" data-required='true' class="form-control" placeholder="Nome" name="nome" id="nome">
								</div>								
								<label id="wrongnome" style="color: red" class="control-label" for="nomeDIV"></label>			
								
								<div id="cognomeDIV"  class="input-group col-xs-12">
									<input  data-required='true' data-format='email' class="form-control" placeholder="Cognome" id="cognome" name="cognome" type="text">
								</div>								
								<label id="wrongcognome" style="color: red" class="control-label" for="cognomeDIV"></label>																	 
								
								<div class="input-group col-xs-12" id="dataDIV">
									<div class="input-group date" id="form_datetime" data-date="1979-09-16T05:25:07Z"
										data-link-field="dtp_input1">
										<input data-format='date' id="data" name="data" data-required='true'
												class="form-control" type="text"
												placeholder="Data di nascita" ondblclick="$(this).val('')">
										<div class="input-group-addon">
											<span class="glyphicon glyphicon-th"></span>
										</div>
									</div>
								</div>
								<label id="wrongdata" style="color: red" class="control-label" for="dataDIV"></label>
								
								<div id="sessoDIV"  class="input-group col-xs-12">
									 <select data-required='true' id="sesso" name="sesso" id="sesso" class="form-control">   
										<option value="seleziona" selected>Sesso</option>
										<option value="M">Maschio</option>
										<option value="F">Femmina</option>		    				
							         </select>
							    </div>								
								<label id="wrongsesso" style="color: red" class="control-label" for="sessoDIV"></label>	
								
								<input class="btn btn-lg btn-success btn-block" type="submit"
									value="Registrati">
							</fieldset>
						</form>
					</div>
				</div>	
			</div>
		</div>
	</div>
</body>
</html>