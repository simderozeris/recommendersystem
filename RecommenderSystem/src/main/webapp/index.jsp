<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<jsp:include page="includes/head.jsp"/>
<body background="images/sfondo.jpg">
<script>
$(function(){
	 $('#login').submit(function(event) {
		 var error = validate($('#login'));
		 if (error != false) {
		 event.preventDefault();
		 }
	 });
});

function validate(form) {
	var error = false;
	
	form.find('input').each(function(index,input) {
		var name = $(input).attr('name');	
		$('#' + name + 'DIV').removeClass("form-group has-error");
		$('#wrong' + name).text("");
	});

	
	form.find('input').each(function(index,input) {
	var inputValue = $(input).val();
	var required = $(input).data('required');
	var format = $(input).data('format');
	var name = $(input).attr('name');
	
	if (required == true && inputValue == "") {
		
		$('#' + name + 'DIV').addClass("form-group has-error");

		$('#wrong' + name).text("Il campo " + name + " e' obbligatorio");
		
		$("#" + name).focus(function () {
			$('#' + name + 'DIV').removeClass("form-group has-error");
			$('#wrong' + name).text("");
        });
		
		error = "error";
				
	}
});
	
	return error;
}
</script>
	<div class="page-header">
		<div class="container">
			<c:if test="${not empty error}">
				<div class="alert alert-danger alert-dismissible" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>				
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					${error}
				</div>
			</c:if>
			<c:if test="${not empty message}">
				<div class="alert alert-success alert-dismissible" role="alert">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>				
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					${message}
				</div>
			</c:if>
		</div>
	</div>

	<div class="container">
		<div class="row vertical-offset-50">
			<div class="col-md-6 col-md-offset-3">
				<div style="text-align:center;">
					<!-- <h1 class="logo-title">Movie 4U</h1>  -->
					 <img alt="Logo" src="images/logo.png" style="margin-bottom: 20px;">
				</div>				
				<div id="panelLogin" class="marginBottom panel panel-default">
					<div class="panel-heading">
						<h1 class="panel-title">Login</h1>
					</div>
					<div class="panel-body">						
						<form id="login" action="/RecommenderSystem/gestione" method="POST">						
							<input id="method" name="method" value="login" data-required='false' type="hidden">								
							<fieldset>
								<div id="emailDIV"  class="form-group">
									<input type="email" data-required='true' data-format='email' class="form-control" placeholder="E-mail" name="email" id="email" type="text">
								</div>	
								<label id="wrongemail" style="color: red" class="control-label" for="emailDIV"></label>																		 								
								<div id="passwordDIV" class="form-group">
									<input id="password" type="password" data-required='true' class="form-control" placeholder="Password" name="password" type="password" value="">
								</div>
								<label id="wrongpassword" style="color: red" class="control-label" for="passwordDIV"></label>																		 								
								<input class="accedi btn btn-lg btn-primary btn-block" type="submit"
									value="Login">
							</fieldset>
						</form>
					</div>
				</div>	
				<a class="linkReg" href="/RecommenderSystem/registra.jsp"><strong>Non sei ancora registrato?</strong></a>		
			</div>
		</div>
	</div>
</body>
</html>