var colors = ["#C62828", "#607D8B", "#9C27B0", "#3949AB", "#AEEA00", "#18FFFF", "#4CAF50", "#FFB300", "#795548", "#212121", "#FF80AB", "#2196F3", "#4DB6AC", "#9E9D24", "#F4FF81", "#BCAAA4", "#E1BEE7", "#FF5722", "#FFEB3B"];

var myColor = d3.scaleSequential().domain([0,6])
  .interpolator(d3.interpolatePuBu);

var selectedYear=0;
var selectedMonth=0;
var selectedGenre=new Set();

// set the dimensions and margins of the graph
var margin = {top: 20, right: 20, bottom: 30, left: 40},
	width = 1200 - margin.left - margin.right,
	height = 550 - margin.top - margin.bottom;

// set the ranges
var x = d3.scaleBand()
		  .range([0, width])
		  .padding(0.3);
var y = d3.scaleLinear()
		  .range([height, 0]);
		  
var xSP;
var ySP;
var svgSP; 
/*		  
const marginSP = {top: 20, right: 20, bottom: 30, left: 40},
widthSP = 960 - marginSP.left - marginSP.right,
heightSP = 400 - marginSP.top - marginSP.bottom;

 svgSP = d3.select("#movie-chart")
 
const xSP = d3.scaleLinear()
.domain([0, 350])
.range([ 0, widthSP ]);
svgSP.append("g")
.attr("transform", `translate(0, ${heightSP})`)
.call(d3.axisBottom(xSP));

// Add Y axis
const ySP = d3.scaleLinear()
.domain([0, 5])
.range([ heightSP, 0]);
svgSP.append("g")
.call(d3.axisLeft(ySP));
*/		  
//tooltip
// var tooltip = d3.select("body")
//		.append("div")
//		.attr("class","d3-tooltip")
//		.style("position", "absolute")
//		.style("z-index", "10")
//		.style("visibility", "hidden")
//		.style("padding", "10px")
//		.style("background", "rgba(0,0,0,0.6)")
//		.style("border-radius", "5px")
//		.style("color", "#fff")
//		.style("line-height", 1)
//		.text("a simple tooltip");


function onClickLogout(){
	d3.select(this).attr('class', 'dropdown open')
	d3.select(".open").on("click", onClickClickLogout);
}

function onClickClickLogout(){
	d3.select(this).attr('class', 'dropdown')
	d3.select(".dropdown").on("click", onClickLogout);

}


		
var tooltip;

// get the data
d3.json("gestione?method=getApprezzamentoGenereTot").then(function(d) {
	d3.select(".dropdown").on("click", onClickLogout);
	keys = Object.keys(d); // chiavi del dataset
	dataValues = Object.values(d);
	dataValues.pop();
	drawInitialBarChart(dataValues);
	
	 tooltip = d3.select("body")
		.append("div")
		.attr("class","d3-tooltip")
		.style("position", "absolute")
		.style("z-index", "10")
		.style("visibility", "hidden")
		.style("padding", "10px")
		.style("background", "rgba(0,0,0,0.6)")
		.style("border-radius", "5px")
		.style("color", "#fff")
		.style("line-height", 1)
		.text("a simple tooltip");
})

function drawInitialBarChart(data){
//d3.csv("data/ApprezzamentoGenere_Complessivo.csv").then(function(data) {

// append the svg object to the body of the page
// append a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3.select("#bar-chart").append("svg")
	.attr("width", width + margin.left + margin.right)
	.attr("height", height + margin.top + margin.bottom)
  .append("g")
	.attr("transform", 
		  "translate(" + margin.left + "," + margin.top + ")");
		  
  // format the data
  data.forEach(function(d) {
	d.avgRating = +d.avgRating;
  });
  // Scale the range of the data in the domains
  x.domain(data.map(function(d) { return d.name; }));
  //y.domain([0, d3.max(data, function(d) { return d.avgRating; })]);
  y.domain([0, 5]);
  
  
function onOverEvent(event, d){
	tooltip.html(`<div>Average rating: <span style='color:red'>${d.avgRating}</span></div><div>Number of ratings: <span style='color:red'> ${d.numVotes}</span></div>`).style("visibility", "visible");
	
	d3.select(this).transition().style("cursor", "pointer").attr("fill-opacity", 0.9)
}

function onMoveEvent(){
	tooltip.style("top", (event.pageY-10)+"px").style("left",(event.pageX+10)+"px");
}

function onOutEvent(){
	tooltip.html(``).style("visibility", "hidden"); 
	d3.select(this).transition().style("cursor", "default").attr("fill-opacity", 0.6)
	}
	
function onClickEvent(){
clicked=d3.select(this).attr("clicked");
if(clicked==0){
	d3.select(this)
	.attr('clicked', 1)
	.attr("stroke", "black")
	.style('stroke-width', 3)
	
	selectedGenre.add(d3.select(this).attr("id"));
	}
else{ d3.select(this)
	.attr('clicked', 0)
	.attr("stroke", "none")
	.style('stroke-width', 0)
	
	selectedGenre.delete(d3.select(this).attr("id"));
	
}
 console.log("Click event end: "+Array.from(selectedGenre));
 updateSP(selectedYear,selectedMonth, selectedGenre, 0);
}



//BAR CHART
  // append the rectangles for the bar chart
  svg.selectAll(".bar")
	  .data(data)
	.enter().append("rect")
	  .attr("class", "bar")
	  .attr("id", function(d) { return d.name; })
	  .attr("x", function(d) { return x(d.name); })
	  .attr("width", x.bandwidth()-5)
	  .attr("y", function(d) { return y(d.avgRating); })
	  //.transition()
	  //.duration(500)
	  .attr("height", function(d) { return height - y(d.avgRating); })
	  .attr("fill", function(d, i) {
			return colors[i];
		})
	  .attr('fill-opacity', 0.6)
	  .attr('clicked', 0)
	  
	  svg.selectAll(".bar")
	  .on("mouseover", onOverEvent)
	  .on("mousemove", onMoveEvent)
	  .on("mouseout", onOutEvent)
	  .on("click", onClickEvent);

  // add the x Axis
  svg.append("g")
	  .attr("transform", "translate(0," + height + ")")
	  .call(d3.axisBottom(x));

  // add the y Axis
  svg.append("g")
	  .call(d3.axisLeft(y));

//});

//SCATTERPLOT
// set the dimensions and margins of the graph
 var marginSP = {top: 20, right: 20, bottom: 30, left: 40},
		widthSP = 960 - marginSP.left - marginSP.right,
		heightSP = 400 - marginSP.top - marginSP.bottom;

// append the svg object to the body of the page
 svgSP = d3.select("#movie-chart")
	.append("svg")
	.attr("id", "movieDetail")
	.attr("width", widthSP + marginSP.left + marginSP.right)
	.attr("height", heightSP + marginSP.top + marginSP.bottom)
	.append("g")
	.attr("transform", `translate(${marginSP.left}, ${marginSP.top})`);

//Read the data
d3.json("gestione?method=getDettaglioMovieTot").then( function(data) {
//console.log("X: "+d3.max(data, function(d) { return d.numVotes; }))
//console.log("Y: "+d3.max(data, function(d) { return d.avgRating; }))
//console.log(data);
	// Add X axis
	//const x = d3.scaleLinear()
	xSP = d3.scaleLinear()
	.domain([0, 350])
	.range([ 0, widthSP ]);
	svgSP.append("g")
	.attr("transform", `translate(0, ${heightSP})`)
	.call(d3.axisBottom(xSP));

	// Add Y axis
	ySP = d3.scaleLinear()
	.domain([0, 5])
	.range([ heightSP, 0]);
	svgSP.append("g")
	.call(d3.axisLeft(ySP));

	// Add dots
	svgSP.append('g')
	.selectAll("dot")
	.data(data)
//	.enter()
	.join("circle")
		.attr("class", "movies" )
		.attr("cx", function (d) { return xSP(d.numVotes); } )
		.attr("cy", function (d) { return ySP(d.avgRating); } )
		.attr("r", 2)
		.style("fill", function(d){return myColor(d.genres.replace(/\s/g, "").split(',').length)})
		.on("mouseover", onOverEventSP)
		.on("mousemove", onMoveEventSP)
		.on("mouseout", onOutEventSP)

})
}



d3.json("gestione?method=getApprezzamentoGenereAnno").then(function(data) {
// List of years
initial=new Set();
initial.add("All years");
var yearsTemp = new Set(data.map(d => d.year));
var years= new Set(Array.from(yearsTemp).reverse());
var allYear=new Set();
initial.forEach(elem => allYear.add(elem));
years.forEach(elem => allYear.add(elem));


// add the options to the button
d3.select("#selectButton")
  .selectAll('myOptions')
	.data(allYear)
  .enter()
	.append('option')
  .text(function (d) { return d; }) // text showed in the menu
  .attr("value", function (d) { return d; }) // corresponding value returned by the button
  
// A function that update the chart
function updateByYear(selectedYear) {

if(selectedYear!="All years"){
	dataFilter = data.filter(function(d){return d.year==selectedYear})
	svg=d3.select("svg")//.remove();
  svg.selectAll('.bar')
  .data(dataFilter)
	  .transition()
	  .duration(500)
	  .attr("y", function(d) { return y(d.avgRating); })
	  .attr("height", function(d) { return height - y(d.avgRating); })
	  .attr("fill", function(d, i) {
			return colors[i];
		})
	  .attr('fill-opacity', 0.6);	  

  
var months =  {
		0: { state: true, name: "month", text: "All months", order: 0 },
		1: { state: false, name: "month", text: "January", order: 1 },
		2: { state: false, name: "month", text: "February", order: 2 },
		3: { state: false, name: "month", text: "March", order: 3 },
		4: { state: false, name: "month", text: "April", order: 4 },
		5: { state: false, name: "month", text: "May", order: 5 },
		6: { state: false, name: "month", text: "June", order: 6 },
		7: { state: false, name: "month", text: "July", order: 7 },
		8: { state: false, name: "month", text: "August", order: 8 },
		9: { state: false, name: "month", text: "September", order: 9 },
		10: { state: false, name: "month", text: "October", order: 10 },
		11: { state: false, name: "month", text: "November", order: 11 },
		12: { state: false, name: "month", text: "December", order: 12, last: true }
	  };

	  
  var radioData = Object.keys(months).map(function(d) { months[d].value = d; return months[d]; }).sort(function(a, b) { return (a.order > b.order ? 1 : (a.order < b.order) ? -1 : 0) })
  //var checkboxData = Object.keys(days).map(function(d) { days[d].value = d; return days[d]; }).sort(function(a, b) { return (a.order > b.order ? 1 : (a.order < b.order) ? -1 : 0) })

var label = d3.select("#monthSelectionDiv").append("label").attr("for"," inputState").attr("class","textMonth")
//label.text("Filtra per mese")
var div = d3.select("#monthSelectionDiv").append("div")
div.attr("class", "btn-group")
div.attr("data-toggle", "buttons")
////var fieldset = div.append("fieldset")
//fieldset.attr("class", "form-horizontal form-label-left")
//var legend = fieldset.append("legend")
//legend.attr("class", "custom-border")
//legend.text("Filter by month");
  
var radioLabel = div.selectAll(".radio")
		.data(radioData)
	  .enter().append("label")
		.attr("class", "btn btn-default")
		.text(function(d) { return d.text });
//		.style("margin-right", function(d) { return d.last == true ? "30px" : "0px" });

  // BoE: add radio button to each span
  radioLabel.append("input")
	  .attr('type', 'radio')
	.attr('name', function(d) { return d.name })
	.attr('value', function(d) { return d.value})
	//.attr('checked',function(d) { return d.state});
	.property('checked', function(d) { return d.state });

  // BoE: add radio button label
  var span = radioLabel.append("span");
  span.style("margin-left"," 5px")
//  span.text(function(d) { return d.text });
  span.attr("class", "glyphicon glyphicon-ok")
  
  d3.selectAll("input[type='radio'][value='0']").select(function(){return this.parentNode;}).attr("class","btn btn-default active")


 // When the radio button is changed, run the updateChart function
d3.selectAll("input[type='radio'][name='month']").on("change", function(event,d) {
	d3.select(".active").attr("class","btn btn-default")
//	console.log("radio event");
	d3.select(this.parentNode).attr("class","btn btn-default active")
	selectedGenre.clear();
	d3.selectAll('.bar')
	.attr('clicked', 0)
	.attr("stroke", "none")
	.style('stroke-width', 0)
	selectedMonth = d3.select(this).property("value")
//	console.log("Year: "+selectedYear+", Month:"+selectedMonth);
	updateByMonth(selectedMonth);
	updateSP(selectedYear,selectedMonth, selectedGenre, 1);
})

function updateByMonth(selectedMonth){
if(selectedMonth!=0){	
d3.json("gestione?method=getApprezzamentoGenereAnnoMese").then(function(data) {
	dataFilter = data.filter(function(d){return (d.year==selectedYear && d.month==selectedMonth)})
	console.log(dataFilter);
	svg=d3.select("svg")//.remove();
  svg.selectAll('.bar')
  .data(dataFilter)
	  .transition()
	  .duration(500)
	  .attr("x", function(d) { return x(d.name); })
	  .attr("width", x.bandwidth()-5)
	  .attr("y", function(d) { return y(d.avgRating); })
	  .attr("height", function(d) { return height - y(d.avgRating); })
	  .attr("fill", function(d, i) {
			return colors[i];
		})
	  .attr('fill-opacity', 0.6);	
	  })
}
else{
	d3.selectAll(".btn-group").remove();
	d3.selectAll(".textMonth").remove();
	updateByYear(selectedYear);
}
	
}

	  
}
else{
	d3.json("gestione?method=getApprezzamentoGenereTot").then(function(d) {
	keys = Object.keys(d); // chiavi del dataset
	dataFilter = Object.values(d);
	dataFilter.pop();

	svg=d3.select("svg")
  svg.selectAll('.bar')
  .data(dataFilter)
	  .transition()
	  .duration(500)
	  .attr("y", function(d) { return y(d.avgRating); })
	  .attr("height", function(d) { return height - y(d.avgRating); })
	  .attr("fill", function(d, i) {
			return colors[i];
		})
	  .attr('fill-opacity', 0.6);
})
}

}

// When the button is changed, run the updateChart function
d3.select("#selectButton").on("change", function(event,d) {
	// recover the option that has been chosen
	d3.selectAll(".btn-group").remove()
	d3.selectAll(".textMonth").remove();

	selectedGenre.clear();
	d3.selectAll('.bar')
	.attr('clicked', 0)
	.attr("stroke", "none")
	.style('stroke-width', 0)
	selectedMonth=0;
	selectedYear = d3.select(this).property("value")
	console.log("Year: "+selectedYear+", Month:"+selectedMonth);
		
//	d3.selectAll("input[type='radio'][name='month']")
//
	// run the updateChart function with this selected option
	updateByYear(selectedYear);
	updateSP(selectedYear,selectedMonth, selectedGenre, 1);
	
//	d3.selectAll("input[type='radio'][value='0']").select(function(){return this.parentNode;}).attr("class","btn btn-default active")
//	//updateSP(selectedYear,selectedMonth);
})
  


})

function onOverEventSP(event, d){
	tooltip
	.html(`<div>Title: <span style='color:red'>${d.title}</span></div><div> Average rating: <span style='color:red'>${d.avgRating}</span></div><div>Number of ratings: <span style='color:red'> ${d.numVotes}</span></div><div>Genres: <span style='color:red'>${d.genres}</span></div>`)
	.style("visibility", "visible");
	
	d3.select(this)
	.transition()
	.style("cursor", "pointer")
	//.attr("fill-opacity", 0.9)
	.attr("r", 4)
}

function onMoveEventSP(){
	tooltip
	.style("top", (event.pageY-10)+"px")
	.style("left",(event.pageX+10)+"px");
}

function onOutEventSP(){
	tooltip
	.html(``)
	.style("visibility", "hidden"); 
	
	d3.select(this)
	.transition()
	.style("cursor", "default")
	//.attr("fill-opacity", 0.6)
	.attr("r", 2)
	}

function updateSP(selectedYear,selectedMonth, selectedGenre, fromEvent){
	selectedGenreArray=Array.from(selectedGenre)
	query="";
	//tutti gli anni
	if(selectedYear==0 || selectedYear=="All years"){
		query="gestione?method=getDettaglioMovieTot"
		//console.log("updateSP: year: "+ selectedYear + ", month: "+ selectedMonth);
		//console.log("selected genres: "+selectedGenreArray);
		d3.json(query).then(function(data) {
		if(fromEvent==0){
		dataFilter = data.filter(function(d){
			genres= d.genres.replace(/\s/g, "").split(',');
			//console.log("Genres: "+genres)

			const contains=genres.some(element => {
  				return selectedGenreArray.includes(element);});
			//console.log("Contains: "+contains)
			return contains;
			})
		}
		else{
			dataFilter=data;
		}
		//console.log(dataFilter);
		//rejoin data
		var circle = svgSP.selectAll("circle").remove()
		
		svgSP
		.append('g')
		.selectAll("dot")
		.data(dataFilter)  
		.join("circle")
		.transition()
		.duration(750)
		.attr("class","movies")
		.attr("cx",function (d) { return xSP(d.numVotes); })
		.attr("cy",function (d) { return ySP(d.avgRating); })
		.attr("r",2)
		.style("fill", function(d){return myColor(d.genres.replace(/\s/g, "").split(',').length)})
		
		svgSP.selectAll("circle")
		.on("mouseover", onOverEventSP)
		.on("mousemove", onMoveEventSP)
		.on("mouseout", onOutEventSP)
	
		})
	}
	//anno selezionato
	else{
		//anno selezionato, mese non selezionato
		if(selectedMonth==0){
		
		query="gestione?method=getDettaglioMovieAnnuale"
		//console.log("updateSP: year: "+ selectedYear + ", month: "+ selectedMonth);
		//console.log("selected genres: "+selectedGenreArray);
		d3.json(query).then(function(data) {
		if(fromEvent==0){
		dataFilter = data.filter(function(d){
			genres= d.genres.replace(/\s/g, "").split(',');
			//console.log("Genres: "+genres)

			const contains=genres.some(element => {
  				return selectedGenreArray.includes(element);});
			//console.log("Contains: "+contains)
			return contains && d.year==selectedYear;
			})
		}
		else {
			dataFilter = data.filter(function(d){ return d.year==selectedYear; })
		}
		//console.log(dataFilter);
		//rejoin data
		var circle = svgSP.selectAll("circle").remove()
		
		svgSP
		.append('g')
		.selectAll("dot")
		.data(dataFilter)  
		.join("circle")
		.transition()
		.duration(750)
		.attr("class","movies")
		.attr("cx",function (d) { return xSP(d.numVotes); })
		.attr("cy",function (d) { return ySP(d.avgRating); })
		.attr("r",2)
		.style("fill", function(d){return myColor(d.genres.replace(/\s/g, "").split(',').length)})
		
		svgSP.selectAll("circle")
		.on("mouseover", onOverEventSP)
		.on("mousemove", onMoveEventSP)
		.on("mouseout", onOutEventSP)
	
		})
		
		}
		//anno selezionato, mese selezionato
		else{
			query="gestione?method=getDettaglioMovieMensile"
		//console.log("updateSP: year: "+ selectedYear + ", month: "+ selectedMonth);
		//console.log("selected genres: "+selectedGenreArray);
		d3.json(query).then(function(data) {
		if(fromEvent==0){
		dataFilter = data.filter(function(d){
			genres= d.genres.replace(/\s/g, "").split(',');
			//console.log("Genres: "+genres)

			const contains=genres.some(element => {
  				return selectedGenreArray.includes(element);});
			//console.log("Contains: "+contains)
			return (contains && d.year==selectedYear && d.month==selectedMonth);
			})
		}
		else {
			dataFilter = data.filter(function(d){ return d.year==selectedYear && d.month==selectedMonth; })
		}
		//console.log(dataFilter);
		//rejoin data
		var circle = svgSP.selectAll("circle").remove()
		
		svgSP
		.append('g')
		.selectAll("dot")
		.data(dataFilter)  
		.join("circle")
		.transition()
		.duration(750)
		.attr("class","movies")
		.attr("cx",function (d) { return xSP(d.numVotes); })
		.attr("cy",function (d) { return ySP(d.avgRating); })
		.attr("r",2)
		.style("fill", function(d){return myColor(d.genres.replace(/\s/g, "").split(',').length)})
		
		svgSP.selectAll("circle")
		.on("mouseover", onOverEventSP)
		.on("mousemove", onMoveEventSP)
		.on("mouseout", onOutEventSP)
	
		})
			
		}
	}	
}