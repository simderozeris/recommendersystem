<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="includes/head.jsp"/>
<%@ page isELIgnored="false" %>
<body background="images/sfondo.jpg">
<script type="text/javascript">
$(function(){
	 
	 $('#registra').submit(function(event) {
		 var error = validate($('#registra'));
		 if (error != false) {	
		 	event.preventDefault();
		 } else {
			 if ($("#password").val() != $("#confermapassword").val()){
				 event.preventDefault();
				 alert("Le password devono corrispondere");
			 }
		 }
	 }); 	 
		var dateCheck1 = /^\d{2}$/;
		var dateCheck2 = /^\d{2}\/\d{2}$/;
		var d = new Date();
		$("#form_datetime").datetimepicker({
			format:'dd/mm/yyyy',
	        startView: 'month',
	        minView: 'month',
			autoclose : true,
			todayBtn : true
		});	
		
		$('#data').off();
		
		$('#data').bind('keypress', function (e) {
	        return !(e.which != 8 && e.which != 0 &&
	                (e.which < 48 || e.which > 57) && e.which != 46);
	    });
		
		
		$("#data").on("change paste keyup", function(e) {
			if (e.which == 0 || (e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105)){
				if(dateCheck1.test($("#data").val()) || dateCheck2.test($("#data").val())){
					$("#data").val($("#data").val() + "/");
				}
			}
		});			 
});

function validate(form) {
	var error = false;
	
	form.find('input').each(function(index, input) {
		var name = $(input).attr('name');	
		$('#' + name + 'DIV').removeClass("has-error");
		$('#wrong' + name).text("");
	});
	
	form.find('select').each(function(index, input) {
		var name = $(input).attr('name');	
		$('#' + name + 'DIV').removeClass("has-error");
		$('#wrong' + name).text("");
	});
	
	form.find('textarea').each(function(index, input) {
		var name = $(input).attr('name');	
		$('#' + name + 'DIV').removeClass("has-error");
		$('#wrong' + name).text("");
	});

	
	form.find('input').each(function(index,input) {
		var inputValue = $(input).val();
		var required = $(input).data('required');
		var format = $(input).data('format');
		var name = $(input).attr('name');
		
		if (required == true && inputValue == "") {
			
			$('#' + name + 'DIV').addClass("form-group has-error");
	
			$('#wrong' + name).text("Il campo " + name + " e' obbligatorio");
			
			$("#" + name).focus(function () {
				$('#' + name + 'DIV').removeClass("form-group has-error");
				$('#wrong' + name).text("");
	        });
			
			error = "error";
					
		}
	});
	
	form.find('select').each(function(index,input) {
		var inputValue = $(input).val();
		var required = $(input).data('required');
		var format = $(input).data('format');
		var name = $(input).attr('name');
		
		if (required == true && inputValue == "seleziona") {
			
			$('#' + name + 'DIV').addClass("form-group has-error");
	
			$('#wrong' + name).text("Il campo " + name + " e' obbligatorio");
			
			$("#" + name).focus(function () {
				$('#' + name + 'DIV').removeClass("form-group has-error");
				$('#wrong' + name).text("");
	        });
			
			error = "error";			
		}
	});
	
	return error;
}


</script>
	<div class="page-header">
		<div class="container">
			<c:if test="${not empty error}">
				<div class="alert alert-danger alert-dismissible" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>				
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					${error}
				</div>
			</c:if>
			<c:if test="${not empty message}">
				<div class="alert alert-success alert-dismissible" role="alert">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>				
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					${message}
				</div>
			</c:if>
		</div>
	</div>
	
	<div class="container">
		<div class="row vertical-offset-50">
			<div class="col-md-6 col-md-offset-3">
				<div style="text-align:center;">
					 <img alt="Logo" src="images/logo.png" style="margin-bottom: 20px;">
				</div>				
				<div id = "panelRegistra" class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Registrati</h3>
					</div>
					<div class="panel-body">
						<form id="registra" action="/RecommenderSystem/gestione" method="POST">						
							<input id="method" name="method" value="registra" data-required='false' type="hidden">	
							<fieldset>
								<div id="emailDIV"  class="form-group">
									<input type="email" data-required='true' data-format='email' class="form-control" placeholder="E-mail" id="email" name="email" type="text">
								</div>								
								<label id="wrongemail" style="color: red" class="control-label" for="emailDIV"></label>				
								
								<div id="passwordDIV" class="form-group">
									<input type="password" data-required='true' class="form-control" placeholder="Password" id="password" name="password" type="password" value="">
								</div>
								<label id="wrongpassword" style="color: red" class="control-label" for="passwordDIV"></label>	
												
								<div id="confermapasswordDIV" class="form-group">
									<input type="password" data-required='true' class="form-control" placeholder="Conferma Password" id="confermapassword" name="confermapassword" value="">
								</div>
								<label id="wrongconfermapassword" style="color: red" class="control-label" for="confermapasswordDIV"></label>																		 
								
								<input class="btn btn-lg btn-success btn-block" type="submit"
									value="Registrati">
							</fieldset>
						</form>
					</div>
				</div>	
			</div>
		</div>
	</div>
</body>
</html>