package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.grouplens.lenskit.ItemRecommender;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.baseline.BaselineScorer;
import org.grouplens.lenskit.baseline.ItemMeanRatingItemScorer;
import org.grouplens.lenskit.baseline.UserMeanBaseline;
import org.grouplens.lenskit.baseline.UserMeanItemScorer;
import org.grouplens.lenskit.core.LenskitConfiguration;
import org.grouplens.lenskit.core.LenskitRecommender;
import org.grouplens.lenskit.data.sql.BasicSQLStatementFactory;
import org.grouplens.lenskit.data.sql.JDBCRatingDAO;
import org.grouplens.lenskit.knn.item.ItemItemScorer;
import org.grouplens.lenskit.transform.normalize.BaselineSubtractingUserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import com.google.gson.Gson;
import it.sl.hibernate.configuration.AppConfig;
import it.sl.hibernate.service.GenreService;
import it.sl.hibernate.service.MovieService;
import it.sl.hibernate.service.RatingService;
import it.sl.hibernate.service.UserService;

public class RecommenderSystemSingleton {
	 private static RecommenderSystemSingleton recommSystSingleton = null; 
	 private ItemRecommender irec = null;
	 AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
	 RatingService ratingService = null;			
	 UserService serviceUser = null;		
	 MovieService movieService = null;
	 GenreService genreService = null;
	 String resultsGenereTot;
	 String resultsGenereAnno;
	 String resultsGenereAnnoMese;
	 String resultsDettaglioMovieTot;
	 String resultsDettaglioMovieAnnuale;
	 String resultsDettaglioMovieMensile;
	 Gson gson = new Gson();;
	  
    public static RecommenderSystemSingleton getInstance() {
        if (recommSystSingleton == null) {
        	recommSystSingleton = new RecommenderSystemSingleton(); 			
        }
        return recommSystSingleton; 
    } 

	public RatingService getRatingService() {
		return ratingService;
	}

	public UserService getServiceUser() {
		return serviceUser;
	}

	public MovieService getMovieService() {
		return movieService;
	}

	public GenreService getGenreService() {
		return genreService;
	}

	public ItemRecommender getIrec() {
		return irec;
	}
	
	public AbstractApplicationContext getContext() {
		return context;
	}
	
	public void setIrec(ItemRecommender irec) {
		this.irec = irec;
	}
	
	public String getResultsGenereTot() {
		return resultsGenereTot;
	}

	public String getResultsGenereAnno() {
		return resultsGenereAnno;
	}

	public String getResultsGenereAnnoMese() {
		return resultsGenereAnnoMese;
	}

	public String getResultsDettaglioMovieTot() {
		return resultsDettaglioMovieTot;
	}

	public String getResultsDettaglioMovieAnnuale() {
		return resultsDettaglioMovieAnnuale;
	}

	public String getResultsDettaglioMovieMensile() {
		return resultsDettaglioMovieMensile;
	}

	public void setServices() {
		ratingService = (RatingService) context.getBean("ratingService");			
		serviceUser = (UserService) context.getBean("userService");		
		movieService = (MovieService) context.getBean("movieService");
		genreService = (GenreService) context.getBean("genreService");
	}


	public void setRecommendSystem(){	
		Config c = Config.getInstance();
		Connection cxn = null;
		
		try {
			Class.forName(c.getProperty("jdbc.driverClassName"));
			cxn  = DriverManager.getConnection(c.getProperty("jdbc.url"),c.getProperty("jdbc.username"),c.getProperty("jdbc.password"));
			cxn.setAutoCommit(true);
			cxn.setAutoCommit(false);
			BasicSQLStatementFactory factory = new BasicSQLStatementFactory();
			factory.setTableName("rating");
			factory.setUserColumn("userId");
			factory.setItemColumn("movieId");
			factory.setRatingColumn("rating");
			factory.setTimestampColumn("timestamp");
			
		    JDBCRatingDAO dao = new JDBCRatingDAO(cxn,factory); 
		    LenskitConfiguration config = new LenskitConfiguration();
		    
		    config.bind(ItemScorer.class).to(ItemItemScorer.class);
			// let's use personalized mean rating as the baseline/fallback predictor.
			// 2-step process:
			// First, use the user mean rating as the baseline scorer
			config.bind(BaselineScorer.class, ItemScorer.class).to(UserMeanItemScorer.class);
			// Second, use the item mean rating as the base for user means
			config.bind(UserMeanBaseline.class, ItemScorer.class).to((Class<? extends ItemScorer>) ItemMeanRatingItemScorer.class);
			// and normalize ratings by baseline prior to computing similarities
			config.bind(UserVectorNormalizer.class).to((Class<? extends UserVectorNormalizer>) BaselineSubtractingUserVectorNormalizer.class);		
		    
		    config.addComponent(dao);
		    /* additional configuration */
		    LenskitRecommender rec = LenskitRecommender.build(config);		    	    
		    
		    RecommenderSystemSingleton.getInstance().setIrec(rec.getItemRecommender());
		    
		} catch (RecommenderBuildException e) {
			e.printStackTrace();
		}  catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
    }
	
	public void setQueryInMemory() {
		resultsGenereTot = gson.toJson(RecommenderSystemSingleton.getInstance().getGenreService().getApprezzamentoGenereTot());
		resultsGenereAnno = gson.toJson(RecommenderSystemSingleton.getInstance().getGenreService().getApprezzamentoGenereAnno());
		resultsGenereAnnoMese = gson.toJson(RecommenderSystemSingleton.getInstance().getGenreService().getApprezzamentoGenereAnnoMese());
		resultsDettaglioMovieTot = gson.toJson(RecommenderSystemSingleton.getInstance().getMovieService().getDettaglioMovieTot());
		resultsDettaglioMovieAnnuale = gson.toJson(RecommenderSystemSingleton.getInstance().getMovieService().getDettaglioMovieAnnuale());
		resultsDettaglioMovieMensile = gson.toJson(RecommenderSystemSingleton.getInstance().getMovieService().getDettaglioMovieMensile());
	}
}
