package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Config {
	private static Logger logger = LogManager.getLogger(Config.class);
	private static Config config = null;
	private Properties configuration;
	
	private Config(String resourcePath) {

		this.configuration = new Properties();
		InputStream in = null;

		try {
			
			in = Config.class.getResourceAsStream(resourcePath);
			this.configuration.load(in);
		} catch (IOException e) {

			logger.error("Errore durante il caricamento della configurazione: " + e.getMessage());
		} finally {

			if (in != null) {

				try { in.close(); } catch (IOException e) { }
			}
		}
	}

	/***
	 * Returns the Config singleton instance
	 * 
	 * @return The Config singleton instance
	 */
	public static Config getInstance() {

		if (config == null) {

			config = new Config("/application.properties");
		}

		return config;
	}

	/***
	 * Returns the configuration
	 * 
	 * @return The configuration
	 */
	public Properties getConfiguration() {

		return this.configuration;
	}
	
	public String getProperty(String name) {
		
		return this.configuration.getProperty(name);
	}
}
