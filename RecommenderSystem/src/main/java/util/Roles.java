package util;

public enum Roles {
	USER("user"),
	COMMERCIAL("commercial");

	private final String text;

	Roles(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}
}
