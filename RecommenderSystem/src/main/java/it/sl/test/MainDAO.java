package it.sl.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import org.grouplens.lenskit.ItemRecommender;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.RatingPredictor;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.baseline.BaselineScorer;
import org.grouplens.lenskit.baseline.ItemMeanRatingItemScorer;
import org.grouplens.lenskit.baseline.UserMeanBaseline;
import org.grouplens.lenskit.baseline.UserMeanItemScorer;
import org.grouplens.lenskit.core.LenskitConfiguration;
import org.grouplens.lenskit.core.LenskitRecommender;
import org.grouplens.lenskit.data.sql.BasicSQLStatementFactory;
import org.grouplens.lenskit.data.sql.JDBCRatingDAO;
import org.grouplens.lenskit.knn.item.ItemItemScorer;
import org.grouplens.lenskit.scored.ScoredId;
import org.grouplens.lenskit.transform.normalize.BaselineSubtractingUserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;

import util.Config;

public class MainDAO {
	public static void main (String args[]) {
		Config c = Config.getInstance();
		Connection cxn = null;				
				
		try {
			
			long startTime = System.nanoTime();			
			
			Class.forName(c.getProperty("jdbc.driverClassName"));
			cxn  = DriverManager.getConnection(c.getProperty("jdbc.url"),c.getProperty("jdbc.username"),c.getProperty("jdbc.password"));
			cxn.setAutoCommit(true);
			cxn.setAutoCommit(false);	
			
			BasicSQLStatementFactory factory = new BasicSQLStatementFactory();
			factory.setTableName("ratings");
			factory.setUserColumn("userId");
			factory.setItemColumn("movieId");
			factory.setRatingColumn("rating");
			factory.setTimestampColumn("timestamp");
			
		    JDBCRatingDAO dao = new JDBCRatingDAO(cxn,factory); 
		    LenskitConfiguration config = new LenskitConfiguration();
		    
		    config.bind(ItemScorer.class).to(ItemItemScorer.class);
			// let's use personalized mean rating as the baseline/fallback predictor.
			// 2-step process:
			// First, use the user mean rating as the baseline scorer
			config.bind(BaselineScorer.class, ItemScorer.class).to(UserMeanItemScorer.class);
			// Second, use the item mean rating as the base for user means
			config.bind(UserMeanBaseline.class, ItemScorer.class).to((Class<? extends ItemScorer>) ItemMeanRatingItemScorer.class);
			// and normalize ratings by baseline prior to computing similarities
			config.bind(UserVectorNormalizer.class).to((Class<? extends UserVectorNormalizer>) BaselineSubtractingUserVectorNormalizer.class);		
		    
		    config.addComponent(dao);
		    /* additional configuration */
		    LenskitRecommender rec = LenskitRecommender.build(config);
		    
		    ItemRecommender irec = rec.getItemRecommender();
		    
		    long endTime = System.nanoTime();    
			long duration = (endTime - startTime);

			System.out.println("TEMPO TRASCORSO PRIMA DEL METODO: " + (double) duration / 1_000_000_000);
		    
			long startTime2 = System.nanoTime();			

			List<ScoredId> recommendations = irec.recommend(617, 5);
			
			long endTime2 = System.nanoTime();    
			long duration2 = (endTime2 - startTime2);

			System.out.println("TEMPO TRASCORSO DOPO IL METODO: " + (double) duration2 / 1_000_000_000);
			
			for (ScoredId item : recommendations) {
				System.out.println("ID: " + item.getId() + " SCORE: " + item.getScore());
			}
			
			RatingPredictor pred = rec.getRatingPredictor();
			double score = pred.predict(1,99117);
			System.out.println("Score predicted for item " + 99117 + " for user " + 1 + ": " + score);
		    
		    
		    /* do things with the recommender */
		} catch (RecommenderBuildException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}	finally {
		    try {
				cxn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
}
}
