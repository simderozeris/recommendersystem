package it.sl.test;

import java.io.File;
import java.util.List;

import org.grouplens.lenskit.ItemRecommender;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.RatingPredictor;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.baseline.BaselineScorer;
import org.grouplens.lenskit.baseline.ItemMeanRatingItemScorer;
import org.grouplens.lenskit.baseline.UserMeanBaseline;
import org.grouplens.lenskit.baseline.UserMeanItemScorer;
import org.grouplens.lenskit.core.LenskitConfiguration;
import org.grouplens.lenskit.core.LenskitRecommender;
import org.grouplens.lenskit.data.dao.EventDAO;
import org.grouplens.lenskit.data.dao.SimpleFileRatingDAO;
import org.grouplens.lenskit.data.sql.JDBCRatingDAO;
import org.grouplens.lenskit.knn.item.ItemItemScorer;
import org.grouplens.lenskit.scored.ScoredId;
import org.grouplens.lenskit.transform.normalize.BaselineSubtractingUserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;

public class Main {
	public static void main (String args[]) {
		LenskitConfiguration config = new LenskitConfiguration();
				// Use item-item CF to score items
				config.bind(ItemScorer.class).to(ItemItemScorer.class);
				// let's use personalized mean rating as the baseline/fallback predictor.
				// 2-step process:
				// First, use the user mean rating as the baseline scorer
				config.bind(BaselineScorer.class, ItemScorer.class).to(UserMeanItemScorer.class);
				// Second, use the item mean rating as the base for user means
				config.bind(UserMeanBaseline.class, ItemScorer.class).to((Class<? extends ItemScorer>) ItemMeanRatingItemScorer.class);
				// and normalize ratings by baseline prior to computing similarities
				config.bind(UserVectorNormalizer.class).to((Class<? extends UserVectorNormalizer>) BaselineSubtractingUserVectorNormalizer.class);
				
				//config.bind(EventDAO.class).to(new SimpleFileRatingDAO(new File("C:\\Users\\simone.derozeris\\Desktop\\ratings.csv"), ","));
				config.bind(EventDAO.class).to(new SimpleFileRatingDAO(new File("C:\\Users\\simone.derozeris\\Desktop\\MovieLensSmall\\ratings.csv"), ","));
				
				LenskitRecommender rec;
				try {
					rec = LenskitRecommender.build(config);
					ItemRecommender irec = rec.getItemRecommender();
					List<ScoredId> recommendations = irec.recommend(1, 5);
					
					for (ScoredId item : recommendations) {
						System.out.println("ID: " + item.getId() + " SCORE: " + item.getScore());
					}
					
					RatingPredictor pred = rec.getRatingPredictor();
					double score = pred.predict(1,99117);
					System.out.println("Score predicted for item " + 99117 + " for user " + 1 + ": " + score);
					
				} catch (RecommenderBuildException e) {
					e.printStackTrace();
				}			
	}
}
