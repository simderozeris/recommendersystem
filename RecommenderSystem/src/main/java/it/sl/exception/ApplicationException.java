package it.sl.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;


public class ApplicationException extends Exception {
private static final long serialVersionUID = 7718828512143293558L;	
	
	Logger logger = Logger.getLogger(ApplicationException.class);

	public ApplicationException(Exception e) {
		super(e.getCause());
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		logger.error(errors.toString());
	}
	
	public ApplicationException(HttpServletRequest request,Exception e) {
		this(e);
		request.setAttribute("error",e);
	}
}
