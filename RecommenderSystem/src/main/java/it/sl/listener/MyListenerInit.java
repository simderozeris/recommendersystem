package it.sl.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;

import util.RecommenderSystemSingleton;

public class MyListenerInit  implements ServletContextListener{

	private Logger logger = Logger.getLogger(MyListenerInit.class);

	// Run recommend system
	public void contextInitialized(ServletContextEvent arg0) {
		logger.debug("Invocato contextInitialized()");
		RecommenderSystemSingleton.getInstance().setRecommendSystem();	
		logger.debug("Terminato setRecommendSystem()");
		RecommenderSystemSingleton.getInstance().setServices();
		logger.debug("Terminato setServices()");
		RecommenderSystemSingleton.getInstance().setQueryInMemory();	
		logger.debug("Terminato setQueryInMemory()");
		logger.debug("Terminato contextInitialized()");
	}

	//Destroy recommend system
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("Invocato contextDestroyed(): Tomcat in stop.");
	}
}