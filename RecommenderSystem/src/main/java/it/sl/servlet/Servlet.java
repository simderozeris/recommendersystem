package it.sl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.grouplens.lenskit.scored.ScoredId;
import org.json.JSONArray;

import com.google.gson.Gson;

import it.sl.exception.ApplicationException;
import it.sl.hibernate.model.Movie;
import it.sl.hibernate.model.Rating;
import it.sl.hibernate.model.User;
import it.sl.resultquery.ResultApprezzamentoGenereAnno;
import it.sl.resultquery.ResultApprezzamentoGenereAnnoMese;
import it.sl.resultquery.ResultApprezzamentoGenereTot;
import it.sl.resultquery.ResultDettaglioMovieAnnuale;
import it.sl.resultquery.ResultDettaglioMovieMensile;
import it.sl.resultquery.ResultDettaglioMovieTot;
import util.RecommenderSystemSingleton;
import util.Roles;

@WebServlet("/gestione")
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Servlet.class);
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			String method = request.getParameter("method") != null  ? request.getParameter("method") : "";						
			
			if(method.equals("login")) {
				String email = request.getParameter("email");
				String password =  request.getParameter("password");
				User user = RecommenderSystemSingleton.getInstance().getServiceUser().findUserByEmail(email);	
	
				if(user != null) {
					if(password.equals(user.getPassword())) {
						logger.info("Login effettuato con successo");	
						response.sendRedirect("/RecommenderSystem/gestione?method=viewHome&userId=" + user.getId());
					} else {
						logger.info("Credenziali errate");
						request.setAttribute("error", "Credenziali errate");
						getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
					}
				} else {	
					logger.info("L'utente non esiste");
					request.setAttribute("error", "L'utente non esiste");
					getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
				}
			} else if (method.equals("logout")){
				request.setAttribute("userName", "");
				request.setAttribute("userId", "");		
				getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
				
			} else if(method.equals("registra")) {
				String email = request.getParameter("email");
				String password =  request.getParameter("password");		
				User user = new User(email,password);
				RecommenderSystemSingleton.getInstance().getServiceUser().saveUser(user);
				response.sendRedirect("/RecommenderSystem/gestione?method=viewHome&userId=" + user.getId());
	
			} else if(method.equals("anagrafica")) {
				int userId = Integer.parseInt(request.getParameter("userId"));

				String nome = request.getParameter("nome");
				String cognome = request.getParameter("cognome");
				Date datanascita = setFormatDate(request.getParameter("data").trim());				
				String sesso = request.getParameter("sesso");
				
				User user = RecommenderSystemSingleton.getInstance().getServiceUser().findUserById(userId);
				user.setNome(nome);
				user.setCognome(cognome);
				user.setSesso(sesso);			
				RecommenderSystemSingleton.getInstance().getServiceUser().updateUser(user);
				response.sendRedirect("/RecommenderSystem/gestione?method=viewHome&userId=" + user.getId());		
				
			} else if(method.equals("viewHome")) {
				int userId = Integer.parseInt(request.getParameter("userId"));
				User user = RecommenderSystemSingleton.getInstance().getServiceUser().findUserById(userId);
				request.setAttribute("userName", user.getEmail());
				request.setAttribute("userId", userId);		
				
				if (user.getRole().equals(Roles.COMMERCIAL.toString())) {
					response.sendRedirect("/RecommenderSystem/gestione?method=homeComm&userId=" + userId);
				} else {

					List<Rating> ratingsByUser = RecommenderSystemSingleton.getInstance().getRatingService().findRatingByIdUser(userId);

					if (ratingsByUser.isEmpty()) {				
						List<Movie> filmPiuVotati = RecommenderSystemSingleton.getInstance().getMovieService().getMovieBestRated();	
						request.setAttribute("movies", filmPiuVotati);		
						request.setAttribute("title", "Per iniziare, lascia una valutazione a questi film");
						getServletContext().getRequestDispatcher("/homeNewUser.jsp").forward(request, response);
					} else {
						if(user.getNome() == null) {
							request.setAttribute("title", "Lasciaci qualche tuo dato anagrafico");
							getServletContext().getRequestDispatcher("/anagrafica.jsp").forward(request, response);
						} else {
							response.sendRedirect("/RecommenderSystem/gestione?method=home&userId=" + userId);
						}
					}
				}
			} else if (method.equals("valutaNewUser")){
				int userId = Integer.parseInt(request.getParameter("userId"));		
				String moviesId = request.getParameter("moviesId");
				String [] moviesIds = moviesId.split(",");
				User user = RecommenderSystemSingleton.getInstance().getServiceUser().findUserById(userId);
				
				for (int i = 0; i<moviesIds.length; i++) {
					String valueRating = request.getParameter("rating" + moviesIds[i]);					
					Rating rating = new Rating(user, RecommenderSystemSingleton.getInstance().getMovieService().findMovieById(Integer.parseInt(moviesIds[i])), Integer.parseInt(valueRating),(int)new Date().getTime());
					RecommenderSystemSingleton.getInstance().getRatingService().saveRating(rating);
				}
				
				resetLenskit();
					    	
				response.sendRedirect("/RecommenderSystem/gestione?method=viewHome&userId=" + userId);
				
			} else if (method.equals("valuta"))   {
				int userId = Integer.parseInt(request.getParameter("userId"));
				JSONArray moviesIds = new JSONArray(request.getParameter("moviesId"));
				User user = RecommenderSystemSingleton.getInstance().getServiceUser().findUserById(userId);
				
				for (int i = 0; i<moviesIds.length(); i++) {
					int moviesId = moviesIds.getJSONObject(i).getInt("movieId");  
					int valueRating = moviesIds.getJSONObject(i).getInt("rating");  				
					Rating rating = new Rating(user, RecommenderSystemSingleton.getInstance().getMovieService().findMovieById(moviesId), valueRating,(int)new Date().getTime());
					RecommenderSystemSingleton.getInstance().getRatingService().saveRating(rating);
				}
				
				resetLenskit();

				response.sendRedirect("/RecommenderSystem/gestione?method=viewHome&userId=" + userId);
				
			} else if(method.equals("home")) {
				int userId = Integer.parseInt(request.getParameter("userId"));		
				List<Movie> movies10Best = RecommenderSystemSingleton.getInstance().getMovieService().get10MovieBestRated();
				List<Movie> movies10BestCurrentYear = RecommenderSystemSingleton.getInstance().getMovieService().get10MovieBestRatedCurrentYear();	
				List<Movie> moviesNotRatedByUser = RecommenderSystemSingleton.getInstance().getMovieService().getMovieNotRatedByUser500(userId);	
				List<ScoredId> recommendations = RecommenderSystemSingleton.getInstance().getIrec().recommend(userId, 5);		
				List<Movie> movieRecommend = new ArrayList<Movie>();
				User user = RecommenderSystemSingleton.getInstance().getServiceUser().findUserById(userId);
				request.setAttribute("userName", user.getEmail());
				request.setAttribute("userId", userId);		
				
				for (ScoredId scoreId : recommendations) {
					Movie movie = RecommenderSystemSingleton.getInstance().getMovieService().findMovieById((int) scoreId.getId());
					movieRecommend.add(movie);
				}

				request.setAttribute("movieRecommend", movieRecommend);		
				request.setAttribute("movies10Best", movies10Best);		
				request.setAttribute("movies10BestCurrentYear", movies10BestCurrentYear);	
				request.setAttribute("moviesNotRatedByUser", moviesNotRatedByUser);					
				request.setAttribute("titoli",setTitoliForAutocomplete(userId));		
								
				getServletContext().getRequestDispatcher("/home.jsp").forward(request, response);

			} else if(method.equals("homeComm")) {
				int userId = Integer.parseInt(request.getParameter("userId"));	
				User user = RecommenderSystemSingleton.getInstance().getServiceUser().findUserById(userId);

				request.setAttribute("userName", user.getEmail());
				request.setAttribute("userId", userId);	
								
				getServletContext().getRequestDispatcher("/homeComm.jsp").forward(request, response);

			} else if(method.equals("getApprezzamentoGenereTot")) {		
				PrintWriter out = response.getWriter();
			    out.write(RecommenderSystemSingleton.getInstance().getResultsGenereTot());  				
			} else if(method.equals("getApprezzamentoGenereAnno")) {
				PrintWriter out = response.getWriter();
			    out.write(RecommenderSystemSingleton.getInstance().getResultsGenereAnno());  					
			} else if(method.equals("getApprezzamentoGenereAnnoMese")) {
				PrintWriter out = response.getWriter();
			    out.write(RecommenderSystemSingleton.getInstance().getResultsGenereAnnoMese());		
			}else if(method.equals("getDettaglioMovieTot")) {
				PrintWriter out = response.getWriter();
			    out.write(RecommenderSystemSingleton.getInstance().getResultsDettaglioMovieTot());		
			}else if(method.equals("getDettaglioMovieAnnuale")) {
				PrintWriter out = response.getWriter();
			    out.write(RecommenderSystemSingleton.getInstance().getResultsDettaglioMovieAnnuale());
			}else if(method.equals("getDettaglioMovieMensile")) {
				PrintWriter out = response.getWriter();
			    out.write(RecommenderSystemSingleton.getInstance().getResultsDettaglioMovieMensile());
			}else if(method.equals("cerca")) {			
				int userId = Integer.parseInt(request.getParameter("userId"));		
				String title = request.getParameter("titolofiltro");
				request.setAttribute("valTitoloFiltro",title);	
				request.setAttribute("userId", userId);		
				request.setAttribute("titoli",setTitoliForAutocomplete(userId));
				List<Movie> moviesNotRatedByUser = null;
				
				if (title.equals("")) {
					moviesNotRatedByUser = RecommenderSystemSingleton.getInstance().getMovieService().getMovieNotRatedByUser500(userId);	
				} else {
					moviesNotRatedByUser = RecommenderSystemSingleton.getInstance().getMovieService().getMovieNotRatedByUserByTitle(userId, title);
				}
				request.setAttribute("moviesNotRatedByUser", moviesNotRatedByUser);
				getServletContext().getRequestDispatcher("/tablemovienotrated.jsp").forward(request, response);
			
			} else {
				logger.info("metodo non riconosciuto");
				request.setAttribute("error", "Errore interno");
				getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
			}

		} catch(Exception e) {
			new ApplicationException(request,e);
			getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	private void resetLenskit() {
		new Thread() {
	        public void run() {
	        	logger.info("reset Lenskit");
	    		RecommenderSystemSingleton.getInstance().setRecommendSystem();	
	        }
	    }.start();
	}
	
	
	private Date setFormatDate(String dateString) throws ParseException {
		Date data = null;
		if(dateString != null && !dateString.equals("")) {
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			data = format.parse(dateString);
		}
		return data;
	}
	
	public String setTitoliForAutocomplete(int userId) {
		String titoli = "";
		List<Movie> allMovie = RecommenderSystemSingleton.getInstance().getMovieService().getMovieNotRatedByUser(userId);
		
		for (Movie movie : allMovie) {
			titoli += movie.getTitolo().replace('\"',' ') + ";";
		}
		
		if(!titoli.isEmpty()) {
			titoli = titoli.substring(0, titoli.length()-1);
		}
		
		return titoli;
	}
}
