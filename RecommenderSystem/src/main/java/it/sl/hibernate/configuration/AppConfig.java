package it.sl.hibernate.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
 
@Configuration
@ComponentScan(basePackages = "it.sl.hibernate")
public class AppConfig {
 
}
