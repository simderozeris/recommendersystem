package it.sl.hibernate.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import it.sl.hibernate.dao.GenreDao;
import it.sl.hibernate.model.Genre;
import it.sl.resultquery.ResultApprezzamentoGenereAnno;
import it.sl.resultquery.ResultApprezzamentoGenereAnnoMese;
import it.sl.resultquery.ResultApprezzamentoGenereTot;

 
@Service("genreService")
@Transactional
public class GenreServiceImpl implements GenreService{
 
    @Autowired
    private GenreDao dao;
     
    @Override
    public void saveGenre(Genre genre) {
        dao.saveGenre(genre);
    }
    
    @Override
    public List<Genre> findAllGenre() {
        return dao.findAllGenre();
    }
 
    @Override
    public void deleteGenreById(int id) {
        dao.deleteGenreById(id);
    }
 
    @Override
    public Genre findGenreById(int id) {
        return dao.findGenreById(id);
    }
    
    @Override
    public Genre findGenreByName(String name) {
        return dao.findGenreByName(name);
    }
 
    @Override
    public void updateGenre(Genre notizia){
    	dao.updateGenre(notizia);
    }
    
    @Override
    public List<ResultApprezzamentoGenereTot> getApprezzamentoGenereTot(){
    	return dao.getApprezzamentoGenereTot();
    }
    
    @Override
	public List<ResultApprezzamentoGenereAnno> getApprezzamentoGenereAnno(){
    	return dao.getApprezzamentoGenereAnno();
    }
	
    @Override
	public List<ResultApprezzamentoGenereAnnoMese> getApprezzamentoGenereAnnoMese(){
    	return dao.getApprezzamentoGenereAnnoMese();
    }
}
