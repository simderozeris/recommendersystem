package it.sl.hibernate.service;

import java.util.List;
import it.sl.hibernate.model.Genre;
import it.sl.resultquery.ResultApprezzamentoGenereAnno;
import it.sl.resultquery.ResultApprezzamentoGenereAnnoMese;
import it.sl.resultquery.ResultApprezzamentoGenereTot;

public interface GenreService {
	
	void saveGenre(Genre genre);
	 
    List<Genre> findAllGenre();
 
    void deleteGenreById(int id);
 
    Genre findGenreById(int id);
    
    Genre findGenreByName(String name);
    
    void updateGenre(Genre genre);
    
    public List<ResultApprezzamentoGenereTot> getApprezzamentoGenereTot();
    
	public List<ResultApprezzamentoGenereAnno> getApprezzamentoGenereAnno();
	
	public List<ResultApprezzamentoGenereAnnoMese> getApprezzamentoGenereAnnoMese();
}

