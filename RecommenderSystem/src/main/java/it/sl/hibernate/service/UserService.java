package it.sl.hibernate.service;

import java.util.List;
import it.sl.hibernate.model.User;

public interface UserService {
	
	void saveUser(User user);
	 
    List<User> findAllUser();
 
    void deleteUserById(int id);
 
    User findUserById(int id);
    
    User findUserByEmail(String nomeutente);
    
    void updateUser(User user);
}

