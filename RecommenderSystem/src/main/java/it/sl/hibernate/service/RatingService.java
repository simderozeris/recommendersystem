package it.sl.hibernate.service;

import java.util.List;
import it.sl.hibernate.model.Rating;

public interface RatingService {
	
	void saveRating(Rating rating);
	 
    List<Rating> findAllRating();
 
    void deleteRatingById(int id);
 
    Rating findRatingById(int id);
         
    List<Rating> findRatingByIdUser(int idUser);
    
    List<Rating> findRatingByIdMovie(int rating);
    
    void updateRating(Rating movie);
}

