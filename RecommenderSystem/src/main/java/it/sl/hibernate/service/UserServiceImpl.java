package it.sl.hibernate.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.sl.hibernate.dao.UserDao;
import it.sl.hibernate.model.User;

 
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
 
    @Autowired
    private UserDao dao;
     
    @Override
    public void saveUser(User user) {
        dao.saveUser(user);
    }
    
    @Override
    public List<User> findAllUser() {
        return dao.findAllUser();
    }
 
    @Override
    public void deleteUserById(int id) {
        dao.deleteUserById(id);
    }
 
    @Override
    public User findUserById(int id) {
        return dao.findUserById(id);
    }
    
    @Override
    public User findUserByEmail(String nomeutente) {
        return dao.findUserByEmail(nomeutente);
    }
 
    @Override
    public void updateUser(User notizia){
        dao.updateUser(notizia);
    }
}
