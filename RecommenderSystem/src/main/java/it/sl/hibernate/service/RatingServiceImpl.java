package it.sl.hibernate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import it.sl.hibernate.dao.RatingDao;
import it.sl.hibernate.model.Rating;

 
@Service("ratingService")
@Transactional
public class RatingServiceImpl implements RatingService{
 
    @Autowired
    private RatingDao dao;
     
    @Override
    public void saveRating(Rating rating) {
        dao.saveRating(rating);
    }
    
    @Override
    public List<Rating> findAllRating() {
        return dao.findAllRating();
    }
 
    @Override
    public void deleteRatingById(int id) {
        dao.deleteRatingById(id);
    }
 
    @Override
    public Rating findRatingById(int id) {
        return dao.findRatingById(id);
    }
    
    @Override
   	public List<Rating> findRatingByIdUser(int IdUser){
           return dao.findRatingByIdUser(IdUser);
    }
    
    @Override
   	public List<Rating> findRatingByIdMovie(int idMovie){
           return dao.findRatingByIdMovie(idMovie);
   }
 
    @Override
    public void updateRating(Rating notizia){
        dao.updateRating(notizia);
    }
}
