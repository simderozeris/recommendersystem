package it.sl.hibernate.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import it.sl.hibernate.dao.MovieDao;
import it.sl.hibernate.model.Movie;
import it.sl.resultquery.ResultDettaglioMovieAnnuale;
import it.sl.resultquery.ResultDettaglioMovieMensile;
import it.sl.resultquery.ResultDettaglioMovieTot;

 
@Service("movieService")
@Transactional
public class MovieServiceImpl implements MovieService{
 
    @Autowired
    private MovieDao dao;
     
    @Override
    public void saveMovie(Movie movie) {
        dao.saveMovie(movie);
    }
    
    @Override
    public List<Movie> findAllMovie() {
        return dao.findAllMovie();
    }
 
    @Override
    public void deleteMovieById(int id) {
        dao.deleteMovieById(id);
    }
 
    @Override
    public Movie findMovieById(int id) {
        return dao.findMovieById(id);
    }
    
    @Override
    public Movie findMovieByTitolo(String titolo) {
        return dao.findMovieByTitolo(titolo);
    }
 
    @Override
    public void updateMovie(Movie notizia){
        dao.updateMovie(notizia);
    }
    
    @Override
    public List<Movie> getMovieBestRated(){
        return dao.getMovieBestRated();
    }
    
    @Override
    public List<Movie> get10MovieBestRated(){
        return dao.get10MovieBestRated();
    }
    
    @Override
    public List<Movie> get10MovieBestRatedCurrentYear(){
        return dao.get10MovieBestRatedCurrentYear();
    }
    
    public List<Movie> getMovieNotRatedByUser(int userId){
    	return dao.getMovieNotRatedByUser(userId);
    }
    
	public List<Movie> getMovieNotRatedByUserByTitle(int userId,String title) {
		return dao.getMovieNotRatedByUserByTitle(userId,title);
	}
	
	public List<Movie> getMovieNotRatedByUser500(int userId){
		return dao.getMovieNotRatedByUser500(userId);
	}
	
	public List<ResultDettaglioMovieTot> getDettaglioMovieTot() {
		return dao.getDettaglioMovieTot();
	}
	
	public List<ResultDettaglioMovieAnnuale> getDettaglioMovieAnnuale(){
		return dao.getDettaglioMovieAnnuale();
	}

	public List<ResultDettaglioMovieMensile> getDettaglioMovieMensile(){
		return dao.getDettaglioMovieMensile();
	}
}
