package it.sl.hibernate.dao;

import java.util.List;
import it.sl.hibernate.model.Movie;
import it.sl.hibernate.model.Rating;
import it.sl.hibernate.model.User;

public interface RatingDao {

	void saveRating(Rating rating);
    
    List<Rating> findAllRating();
     
    void deleteRatingById(int id);
     
    Rating findRatingById(int id);
    
    List<Rating> findRatingByIdUser(int idUser);
    
    List<Rating> findRatingByIdMovie(int idMovie);
    
    void updateRating(Rating rating);
}

