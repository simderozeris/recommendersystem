package it.sl.hibernate.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import com.mysql.cj.xdevapi.Result;

import it.sl.hibernate.model.Genre;
import it.sl.hibernate.model.Movie;
import it.sl.resultquery.ResultApprezzamentoGenereAnno;
import it.sl.resultquery.ResultApprezzamentoGenereAnnoMese;
import it.sl.resultquery.ResultApprezzamentoGenereTot;
  
@Repository("genreDao")
public class GenreDaoImpl extends AbstractDao implements GenreDao{
 
	@Override
	public void saveGenre(Genre genre) {
        persist(genre);
    }

	@Override
    @SuppressWarnings("unchecked")
	public List<Genre> findAllGenre(){
        Criteria criteria = getSession().createCriteria(Genre.class).addOrder(Order.asc("id"));
        return (List<Genre>) criteria.list();   		
    }
 
	@Override
	public void deleteGenreById(int id) {
        Query query = getSession().createSQLQuery("delete from genre where id = :id");
        query.setInteger("id", id);
        query.executeUpdate();    
    }
 
	@Override
	public Genre findGenreById(int id) {
        Criteria criteria = getSession().createCriteria(Genre.class);
        criteria.add(Restrictions.eq("id",id));
        return (Genre) criteria.uniqueResult();
    }
	
	@Override
	public Genre findGenreByName(String name) {
        Criteria criteria = getSession().createCriteria(Genre.class);
        criteria.add(Restrictions.eq("name",name));
        return (Genre) criteria.uniqueResult();
    }
	
	@Override
	public void updateGenre(Genre genre) {
        getSession().update(genre);
    } 
	
	@Override
    @SuppressWarnings("unchecked")
	public List<ResultApprezzamentoGenereTot> getApprezzamentoGenereTot(){
		String queryString = "select g.name, ROUND(AVG(rating),2) avgRating, count(1) numVotes\r\n"
				+ "FROM relmoviegenre r JOIN movie m ON r.movieId=m.movieId JOIN genre g ON r.genreId=g.genreId JOIN rating ra ON m.movieId=ra.movieId \r\n"
				+ "GROUP BY g.name\r\n"
				+ "ORDER BY g.name";
      
//		Query query = getSession().createQuery(queryString);
//		return (List<ResultApprezzamentoGenereTot>) query.list();
		
		
		
		List<ResultApprezzamentoGenereTot> results = getSession().createSQLQuery(queryString)
				.addScalar("name", new StringType())
		        .addScalar("avgRating", new DoubleType())
		        .addScalar("numVotes", new IntegerType())
				.setResultTransformer(Transformers.aliasToBean(ResultApprezzamentoGenereTot.class))
		        .list();
		
		return results;	
	}
	
	@Override
    @SuppressWarnings("unchecked")
	public List<ResultApprezzamentoGenereAnno> getApprezzamentoGenereAnno(){
		String queryString = "select YEAR(ra.ratingDate) year, g.name, ROUND(AVG(rating),2) avgRating, count(1) numVotes\r\n"
				+ "FROM relmoviegenre r JOIN movie m ON r.movieId=m.movieId JOIN genre g ON r.genreId=g.genreId JOIN rating ra ON m.movieId=ra.movieId \r\n"
				+ "GROUP BY g.name, YEAR(ra.ratingDate)\r\n"
				+ "ORDER BY g.name\r\n"
				+ "";
		
		List<ResultApprezzamentoGenereAnno> results = getSession().createSQLQuery(queryString)
		        .addScalar("year", new IntegerType())
				.addScalar("name", new StringType())
		        .addScalar("avgRating", new DoubleType())
		        .addScalar("numVotes", new IntegerType())
				.setResultTransformer(Transformers.aliasToBean(ResultApprezzamentoGenereAnno.class))
		        .list();
        

		return results;
	}
	
	@Override
    @SuppressWarnings("unchecked")
	public List<ResultApprezzamentoGenereAnnoMese> getApprezzamentoGenereAnnoMese(){
		String queryString = "select temp1.*, IFNULL(avgRating,0) avgRating, IFNULL(numVotes,0) numVotes\r\n"
				+ "from (\r\n"
				+ "select * from\r\n"
				+ "(select year, month, name FROM\r\n"
				+ "(select distinct(YEAR(ratingDate)) year from rating order by YEAR(ratingDate)) years, (select distinct(MONTH(ratingDate)) month from rating order by MONTH(ratingDate)) months, genre) temp\r\n"
				+ ") temp1 LEFT JOIN \r\n"
				+ "(select YEAR(ratingDate) year, MONTH(ratingDate) month, g.name, ROUND(avg(r.rating),2) avgRating, count(1) numVotes\r\n"
				+ "from rating r JOIN relmoviegenre rel ON r.movieId=rel.movieId JOIN genre g ON rel.genreId=g.genreId\r\n"
				+ "GROUP BY g.name, year, month\r\n"
				+ "order by year, month, g.name) temp2\r\n"
				+ "ON temp1.year=temp2.year AND temp1.month=temp2.month AND temp1.name=temp2.name\r\n"
				+ "";
		
		List<ResultApprezzamentoGenereAnnoMese> results = getSession().createSQLQuery(queryString)
		        .addScalar("year", new IntegerType())
		        .addScalar("month", new IntegerType())
				.addScalar("name", new StringType())
		        .addScalar("avgRating", new DoubleType())
		        .addScalar("numVotes", new IntegerType())
				.setResultTransformer(Transformers.aliasToBean(ResultApprezzamentoGenereAnnoMese.class))
		        .list();
		
		return results;

	}		
}
