package it.sl.hibernate.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import it.sl.hibernate.model.User;
  
@Repository("userDao")
public class UserDaoImpl extends AbstractDao implements UserDao{
 
	@Override
	public void saveUser(User user) {
        persist(user);
    }

	@Override
    @SuppressWarnings("unchecked")
	public List<User> findAllUser(){
        Criteria criteria = getSession().createCriteria(User.class).addOrder(Order.asc("userId"));
        return (List<User>) criteria.list();   		
    }
 
	@Override
	public void deleteUserById(int id) {
        Query query = getSession().createSQLQuery("delete from user where id = :id");
        query.setInteger("id", id);
        query.executeUpdate();    
    }
 
	@Override
	public User findUserById(int id) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("id",id));
        return (User) criteria.uniqueResult();
    }
	
	@Override
	public User findUserByEmail(String email) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("email",email));
        return (User) criteria.uniqueResult();
    }
	
	@Override
	public void updateUser(User user) {
        getSession().update(user);
    }     
}
