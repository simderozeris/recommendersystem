package it.sl.hibernate.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import it.sl.hibernate.model.Rating;
  
@Repository("ratingDao")
public class RatingDaoImpl extends AbstractDao implements RatingDao{
 
	@Override
	public void saveRating(Rating rating) {
        persist(rating);
    }

	@Override
    @SuppressWarnings("unchecked")
	public List<Rating> findAllRating(){
        Criteria criteria = getSession().createCriteria(Rating.class).addOrder(Order.asc("id"));
        return (List<Rating>) criteria.list();   		
    }
 
	@Override
	public void deleteRatingById(int id) {
        Query query = getSession().createSQLQuery("delete from rating where id = :id");
        query.setInteger("id", id);
        query.executeUpdate();    
    }
 
	@Override
	public Rating findRatingById(int id) {
        Criteria criteria = getSession().createCriteria(Rating.class);
        criteria.add(Restrictions.eq("id",id));
        return (Rating) criteria.uniqueResult();
    }
	
	@Override
	public List<Rating> findRatingByIdUser(int id){
		Criteria criteria = getSession().createCriteria(Rating.class);
        criteria.add(Restrictions.eq("user.id",id));
        return (List<Rating>) criteria.list();
    }
	
	@Override
	public List<Rating> findRatingByIdMovie(int id){
		Criteria criteria = getSession().createCriteria(Rating.class);
        criteria.add(Restrictions.eq("movie.id",id));
        return (List<Rating>) criteria.list();
    }
	
	@Override
	public void updateRating(Rating rating) {
        getSession().update(rating);
    }     
}
