package it.sl.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import it.sl.hibernate.model.Movie;
import it.sl.resultquery.ResultApprezzamentoGenereTot;
import it.sl.resultquery.ResultDettaglioMovieAnnuale;
import it.sl.resultquery.ResultDettaglioMovieMensile;
import it.sl.resultquery.ResultDettaglioMovieTot;
  
@Repository("movieDao")
public class MovieDaoImpl extends AbstractDao implements MovieDao{
 
	@Override
	public void saveMovie(Movie movie) {
        persist(movie);
    }

	@Override
    @SuppressWarnings("unchecked")
	public List<Movie> findAllMovie(){
        Criteria criteria = getSession().createCriteria(Movie.class).addOrder(Order.asc("id"));
        return (List<Movie>) criteria.list();   		
    }
 
	@Override
	public void deleteMovieById(int id) {
        Query query = getSession().createSQLQuery("delete from movie where id = :id");
        query.setInteger("id", id);
        query.executeUpdate();    
    }
 
	@Override
	public Movie findMovieById(int id) {
        Criteria criteria = getSession().createCriteria(Movie.class);
        criteria.add(Restrictions.eq("id",id));
        return (Movie) criteria.uniqueResult();
    }
	
	@Override
	public Movie findMovieByTitolo(String titolo) {
        Criteria criteria = getSession().createCriteria(Movie.class);
        criteria.add(Restrictions.eq("titolo",titolo));
        return (Movie) criteria.uniqueResult();
    }
	
	@Override
	public void updateMovie(Movie movie) {
        getSession().update(movie);
    } 
	
	@Override
	public List<Movie> getMovieBestRated(){
		String queryString = "SELECT m.movieId, m.title, m.genres FROM (SELECT * FROM rating r group by r.movieId order by count(1) desc limit 5) result JOIN movie m ON result.movieId = m.movieId;";
        Query query = getSession().createSQLQuery(queryString).addEntity(Movie.class);    
        return (List<Movie>) query.list();
	}
	
	@Override
	public List<Movie> get10MovieBestRated(){
		String queryString = "select m.movieId, m.title, m.genres FROM (SELECT *, avg(r.rating) as ratingAvg FROM rating r group by r.movieId having count(1)>10) result join movie m on result.movieId=m.movieId order by result.ratingAvg desc limit 10";
        Query query = getSession().createSQLQuery(queryString).addEntity(Movie.class);    
        return (List<Movie>) query.list();
	}
	
	@Override
	public List<Movie> get10MovieBestRatedCurrentYear(){
		String queryString = "select m.movieId, m.title, m.genres FROM (SELECT *, avg(r.rating) as ratingAvg FROM rating r where FROM_UNIXTIME(r.timestamp , '%Y')=2018 group by r.movieId having count(1)>10) result join movie m on result.movieId=m.movieId order by result.ratingAvg desc limit 10";
        Query query = getSession().createSQLQuery(queryString).addEntity(Movie.class);    
        return (List<Movie>) query.list();
	}
	
	@Override
	public List<Movie> getMovieNotRatedByUser(int userId){
		String queryString = "select distinct m.movieId, m.title, m.genres FROM movie m where m.movieId not in (select r.movieId from rating r where r.userId =:userId)";
        Query query = getSession().createSQLQuery(queryString).addEntity(Movie.class);
        query.setInteger("userId", userId);
        return (List<Movie>) query.list();
	}
	
	@Override
	public List<Movie> getMovieNotRatedByUser500(int userId){
		String queryString = "select distinct m.movieId, m.title, m.genres FROM movie m where m.movieId not in (select r.movieId from rating r where r.userId =:userId) limit 500";
        Query query = getSession().createSQLQuery(queryString).addEntity(Movie.class);
        query.setInteger("userId", userId);
        return (List<Movie>) query.list();
	}
	
	@Override
	public List<Movie> getMovieNotRatedByUserByTitle(int userId, String title){
		String queryString = "select distinct m.movieId, m.title, m.genres FROM movie m where m.title LIKE :title AND m.movieId not in (select r.movieId from rating r where r.userId =:userId)";
        Query query = getSession().createSQLQuery(queryString).addEntity(Movie.class);
        query.setInteger("userId", userId);
        query.setString("title", title + "%");
        return (List<Movie>) query.list();
	}
	
	@Override
    @SuppressWarnings("unchecked")
	public List<ResultDettaglioMovieTot> getDettaglioMovieTot(){
		String queryString = "select ROUND(avg(r.rating),2) avgRating, count(1) numVotes, REPLACE(m.title,',','') title, REPLACE(m.genres,'|',', ') genres\r\n"
				+ "from rating r, movie m\r\n"
				+ "where r.movieId=m.movieId and genres !='(no genres listed)'\r\n"
				+ "group by m.movieId\r\n"
				+ "order by m.title\r\n"
				+ "";
		
		List<ResultDettaglioMovieTot> results = getSession().createSQLQuery(queryString)
		        .addScalar("avgRating", new DoubleType())
		        .addScalar("numVotes", new IntegerType())
				.addScalar("title", new StringType())
				.addScalar("genres", new StringType())
				.setResultTransformer(Transformers.aliasToBean(ResultDettaglioMovieTot.class))
		        .list();
		
		return results;	
	}
	
	@Override
    @SuppressWarnings("unchecked")
	public List<ResultDettaglioMovieAnnuale> getDettaglioMovieAnnuale(){
		String queryString = "select YEAR(r.ratingDate) year, ROUND(avg(r.rating),2) avgRating, count(1) numVotes, REPLACE(m.title,',','') title, REPLACE(m.genres,'|',', ') genres\r\n"
				+ "from rating r, movie m\r\n"
				+ "where r.movieId=m.movieId and genres !='(no genres listed)'\r\n"
				+ "group by YEAR(r.ratingDate), m.movieId\r\n"
				+ "order by year, m.title\r\n"
				+ "";
		
		List<ResultDettaglioMovieAnnuale> results = getSession().createSQLQuery(queryString)
		        .addScalar("year", new IntegerType())
				.addScalar("avgRating", new DoubleType())
		        .addScalar("numVotes", new IntegerType())
				.addScalar("title", new StringType())
				.addScalar("genres", new StringType())
				.setResultTransformer(Transformers.aliasToBean(ResultDettaglioMovieAnnuale.class))
		        .list();
		
		return results;	
	}
	
	@Override
    @SuppressWarnings("unchecked")
	public List<ResultDettaglioMovieMensile> getDettaglioMovieMensile(){
		String queryString = "select YEAR(r.ratingDate) year, MONTH(r.ratingDate) month, ROUND(avg(r.rating),2) avgRating, count(1) numVotes, REPLACE(m.title,',','') title, REPLACE(m.genres,'|',', ') genres\r\n"
				+ "from rating r, movie m\r\n"
				+ "where r.movieId=m.movieId and genres !='(no genres listed)'\r\n"
				+ "group by YEAR(r.ratingDate), MONTH(r.ratingDate), m.movieId\r\n"
				+ "order by year, month, m.title\r\n"
				+ "";
		
		List<ResultDettaglioMovieMensile> results = getSession().createSQLQuery(queryString)
		        .addScalar("year", new IntegerType())
		        .addScalar("month", new IntegerType())
				.addScalar("avgRating", new DoubleType())
		        .addScalar("numVotes", new IntegerType())
				.addScalar("title", new StringType())
				.addScalar("genres", new StringType())
				.setResultTransformer(Transformers.aliasToBean(ResultDettaglioMovieMensile.class))
		        .list();
		
		return results;	
	}
}
