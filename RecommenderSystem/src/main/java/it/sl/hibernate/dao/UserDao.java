package it.sl.hibernate.dao;

import java.util.List;
import it.sl.hibernate.model.User;

public interface UserDao {

	void saveUser(User user);
    
    List<User> findAllUser();
     
    void deleteUserById(int id);
     
    User findUserById(int id);
    
    User findUserByEmail(String email);
    
    void updateUser(User user);
}

