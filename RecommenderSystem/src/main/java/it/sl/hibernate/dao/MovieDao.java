package it.sl.hibernate.dao;

import java.util.List;
import it.sl.hibernate.model.Movie;
import it.sl.resultquery.ResultDettaglioMovieAnnuale;
import it.sl.resultquery.ResultDettaglioMovieMensile;
import it.sl.resultquery.ResultDettaglioMovieTot;

public interface MovieDao {

	void saveMovie(Movie movie);
    
    List<Movie> findAllMovie();
     
    void deleteMovieById(int id);
     
    Movie findMovieById(int id);
    
    Movie findMovieByTitolo(String titolo);
    
    void updateMovie(Movie movie);
    
    List<Movie> getMovieBestRated();
    
    List<Movie> get10MovieBestRated();

    List<Movie> get10MovieBestRatedCurrentYear();
    
    List<Movie> getMovieNotRatedByUser(int userId);
    
	List<Movie> getMovieNotRatedByUserByTitle(int userId, String title);
	
	List<Movie> getMovieNotRatedByUser500(int userId);
	
	List<ResultDettaglioMovieTot> getDettaglioMovieTot();
	
	List<ResultDettaglioMovieAnnuale> getDettaglioMovieAnnuale();

	List<ResultDettaglioMovieMensile> getDettaglioMovieMensile();

}

