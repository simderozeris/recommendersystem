package it.sl.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "userId", unique=true, nullable = false)
	private int id;

	@Column(name = "first_name", unique = false, nullable = true)
	private String nome;
	
	@Column(name = "last_name", unique = false, nullable = true)
	private String cognome;
	
	@Column(name = "email", unique = false, nullable = false)
	private String email;
	
	@Column(name = "gender", unique = false, nullable = true)
	private String sesso;
	
	@Column(name = "password", unique = false, nullable = false)
	private String password;	

	@Column(name = "role", unique = false, nullable = false)
	private String role;	

	public User(int id, String nome, String cognome, String sesso, String email, String password, String role) {
		super();
		this.id = id;
		this.nome = nome;
		this.cognome = cognome;
		this.sesso = sesso;
		this.email = email;
		this.password = password;
		this.role = role;
	}

	
	public User(String nome, String cognome, String sesso, String email, String password, String role) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.sesso = sesso;
		this.email = email;
		this.password = password;
		this.role = role;
	}
	
	public User(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public User() {
		super();
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}


	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	public String getSesso() {
		return sesso;
	}

	public void setSesso(String sesso) {
		this.sesso = sesso;
	}

	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}
}
