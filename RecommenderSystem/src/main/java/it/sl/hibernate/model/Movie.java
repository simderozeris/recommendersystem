package it.sl.hibernate.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="movie")
public class Movie {

	@Id
	@Column(name = "movieId", unique=true, nullable = false)
	private int id;

	@Column(name = "title", unique = false, nullable = false)
	private String titolo;
	
	@Column(name = "genres", unique = false, nullable = false)
	private String genere;
	
	@ManyToMany(mappedBy = "movies", fetch = FetchType.LAZY)
	Set<Genre> genresList = new HashSet<>();
	
	public Movie() {
		super();
	}

	public Movie(String titolo, String genere, Set<Genre> genres) {
		super();
		this.titolo = titolo;
		this.genere = genere;
		this.genresList = genres;
	}
	

	public Movie(int id, String titolo, String genere, Set<Genre> genres) {
		super();
		this.id = id;
		this.titolo = titolo;
		this.genere = genere;
		this.genresList = genres;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getGenere() {
		return genere.replace('|',',').replaceAll("[,]", "$0 ");
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	public Set<Genre> getGenresList() {
		return genresList;
	}

	public void setGenresList(Set<Genre> genresList) {
		this.genresList = genresList;
	}
}
