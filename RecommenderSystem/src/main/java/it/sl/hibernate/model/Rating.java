package it.sl.hibernate.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="rating")
public class Rating {

	@Id
	@Column(name = "ratingId", unique=true, nullable = false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

    @OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="userId")
	private User user;
	
    @OneToOne(fetch = FetchType.EAGER)
   	@JoinColumn(name="movieId")
	private Movie movie;
    
    @Column(name = "rating", nullable = false)
    private int rating;
    
    @Column(name = "timestamp", nullable = false)
    private int timestamp;
    
	 @Column(name = "ratingDate", unique = false, nullable = true)
	 @Temporal(TemporalType.DATE)
	private Date dataRating;
    
	public Rating() {
		super();
	}
	
	public Rating(User user, Movie movie, int rating, int timestamp,Date dataRating) {
		super();
		this.user = user;
		this.movie = movie;
		this.rating = rating;
		this.timestamp = timestamp;
		this.dataRating = dataRating;
	}	
	
	public Rating(User user, Movie movie, int rating, int timestamp) {
		super();
		this.user = user;
		this.movie = movie;
		this.rating = rating;
		this.timestamp = timestamp;
	}	

	public Rating(int id, User user, Movie movie, int rating, int timestamp,Date dataRating) {
		super();
		this.id = id;
		this.user = user;
		this.movie = movie;
		this.rating = rating;
		this.timestamp = timestamp;
		this.dataRating = dataRating;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}

	public Date getDataRating() {
		return dataRating;
	}

	public void setDataRating(Date dataRating) {
		this.dataRating = dataRating;
	}
}
