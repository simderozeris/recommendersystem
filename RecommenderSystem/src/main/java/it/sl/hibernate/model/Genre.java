package it.sl.hibernate.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="genre")
public class Genre {

	@Id
	@Column(name = "genreId", unique=true, nullable = false)
	private int id;

	@Column(name = "name", unique = false, nullable = false)
	private String name;
	
	@ManyToMany(cascade = {CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinTable(name = "relmoviegenre", joinColumns = {@JoinColumn(name="genreId",insertable=false, updatable=false)},
									   inverseJoinColumns =  {@JoinColumn(name="movieId",insertable=false, updatable=false)})
	Set<Movie> movies = new HashSet<>();

	public Genre() {
		super();
	}
	
	public Genre(String name,Set<Movie> movies) {
		super();
		this.name = name;
		this.movies = movies;
	}
	

	public Genre(int id, String name,Set<Movie> movies) {
		super();
		this.id = id;
		this.name = name;
		this.movies = movies;
	}

	public int getGenreId() {
		return id;
	}

	public void setGenreId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Movie> getMovies() {
		return movies;
	}

	public void setMovies(Set<Movie> movies) {
		this.movies = movies;
	}
}
