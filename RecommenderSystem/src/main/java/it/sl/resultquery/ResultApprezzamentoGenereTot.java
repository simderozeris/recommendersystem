package it.sl.resultquery;

public class ResultApprezzamentoGenereTot {
	
	private String name;
	private Double avgRating;
	private Integer numVotes;
	
	public ResultApprezzamentoGenereTot() {
		super();
	}

	public ResultApprezzamentoGenereTot(String name, Double avgRating, Integer numVotes) {
		super();
		this.name = name;
		this.avgRating = avgRating;
		this.numVotes = numVotes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(Double avgRating) {
		this.avgRating = avgRating;
	}

	public Integer getNumVotes() {
		return numVotes;
	}

	public void setNumVotes(Integer numVotes) {
		this.numVotes = numVotes;
	}
}
