package it.sl.resultquery;

public class ResultApprezzamentoGenereAnno {
	
	private Integer year;
	private String name;
	private Double avgRating;
	private Integer numVotes;
	
	public ResultApprezzamentoGenereAnno() {
		super();
	}

	public ResultApprezzamentoGenereAnno(Integer year, String name, Double avgRating, Integer numVotes) {
		super();
		this.year = year;
		this.name = name;
		this.avgRating = avgRating;
		this.numVotes = numVotes;
	}
	
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(Double avgRating) {
		this.avgRating = avgRating;
	}

	public Integer getNumVotes() {
		return numVotes;
	}

	public void setNumVotes(Integer numVotes) {
		this.numVotes = numVotes;
	}
}
