package it.sl.resultquery;

public class ResultDettaglioMovieTot {
	
	private Double avgRating;
	private Integer numVotes;
	private String title;
	private String genres;


	public ResultDettaglioMovieTot() {
		super();
	}

	public ResultDettaglioMovieTot(Double avgRating, Integer numVotes, String title, String genres) {
		super();
		this.avgRating = avgRating;
		this.numVotes = numVotes;
		this.title = title;
		this.genres = genres;
	}

	public Double getAvgRating() {
		return avgRating;
	}


	public void setAvgRating(Double avgRating) {
		this.avgRating = avgRating;
	}


	public Integer getNumVotes() {
		return numVotes;
	}


	public void setNumVotes(Integer numVotes) {
		this.numVotes = numVotes;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getGenres() {
		return genres;
	}


	public void setGenres(String genres) {
		this.genres = genres;
	}
}
