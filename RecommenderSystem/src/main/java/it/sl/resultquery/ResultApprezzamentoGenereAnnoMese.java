package it.sl.resultquery;

public class ResultApprezzamentoGenereAnnoMese {
	private Integer year;
	private Integer month;
	private String name;
	private Double avgRating;
	private Integer numVotes;
	
	public ResultApprezzamentoGenereAnnoMese() {
		super();
	}

	public ResultApprezzamentoGenereAnnoMese(Integer year, Integer month, String name, Double avgRating, Integer numVotes) {
		super();
		this.year = year;
		this.month = month;
		this.name = name;
		this.avgRating = avgRating;
		this.numVotes = numVotes;
	}
	
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(Double avgRating) {
		this.avgRating = avgRating;
	}

	public Integer getNumVotes() {
		return numVotes;
	}

	public void setNumVotes(Integer numVotes) {
		this.numVotes = numVotes;
	}
}
