package it.sl.resultquery;

public class ResultDettaglioMovieMensile {
	
	private Integer year;
	private Integer month;
	private Double avgRating;
	private Integer numVotes;
	private String title;
	private String genres;


	public ResultDettaglioMovieMensile() {
		super();
	}

	public ResultDettaglioMovieMensile(Integer year, Integer month, Double avgRating, Integer numVotes, String title, String genres) {
		super();
		this.year = year;
		this.month = month;
		this.avgRating = avgRating;
		this.numVotes = numVotes;
		this.title = title;
		this.genres = genres;
	}

	public Integer getYear() {
		return year;
	}


	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Double getAvgRating() {
		return avgRating;
	}


	public void setAvgRating(Double avgRating) {
		this.avgRating = avgRating;
	}


	public Integer getNumVotes() {
		return numVotes;
	}


	public void setNumVotes(Integer numVotes) {
		this.numVotes = numVotes;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getGenres() {
		return genres;
	}


	public void setGenres(String genres) {
		this.genres = genres;
	}
}
