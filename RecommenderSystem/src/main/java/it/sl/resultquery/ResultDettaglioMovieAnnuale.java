package it.sl.resultquery;

public class ResultDettaglioMovieAnnuale {
	
	private Integer year;
	private Double avgRating;
	private Integer numVotes;
	private String title;
	private String genres;


	public ResultDettaglioMovieAnnuale() {
		super();
	}

	public ResultDettaglioMovieAnnuale(Integer year, Double avgRating, Integer numVotes, String title, String genres) {
		super();
		this.year = year;
		this.avgRating = avgRating;
		this.numVotes = numVotes;
		this.title = title;
		this.genres = genres;
	}

	public Integer getYear() {
		return year;
	}


	public void setYear(Integer year) {
		this.year = year;
	}


	public Double getAvgRating() {
		return avgRating;
	}


	public void setAvgRating(Double avgRating) {
		this.avgRating = avgRating;
	}


	public Integer getNumVotes() {
		return numVotes;
	}


	public void setNumVotes(Integer numVotes) {
		this.numVotes = numVotes;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getGenres() {
		return genres;
	}


	public void setGenres(String genres) {
		this.genres = genres;
	}
}
